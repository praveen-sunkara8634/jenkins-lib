#!/bin/groovy

package dxp.decurtis;

def token
def commitId
def jobName
def buildNumber
def bitBucketRepo
def statusCheck

def setVariables(def latestCommitId, def bitBucketRepoName, def statusCheckFlag) {
    commitId=latestCommitId
    jobName = JOB_NAME
    buildNumber = BUILD_NUMBER
    bitBucketRepo = bitBucketRepoName
    statusCheck = statusCheckFlag

}

def dotNetCodeCompile(def compiledProjectName) {
    try {
        stage("Code Compilation") {
            sh"""
                dotnet publish ${bitBucketRepo}/${compiledProjectName}/${compiledProjectName}.csproj -c Release -o $WORKSPACE/${bitBucketRepo}/publish/
            """
        }
    } catch (e) {
        println e
        if (statusCheck == 'true') {
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Code Compilation failed")
        }
        throw e
    }
}

def nodeCodeCompile() {
    try {
        stage("Code Compilation") {
            dir (bitBucketRepo) {
                sh """
                    npm install
                    npm run test                
                """
            }
        }
    } catch (e) {
        println e
        if (statusCheck == 'true') {
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Code Compilation failed")
        }
        throw e
    }
}

def javaCodeCompile(def javaVersion) {
    try {
        stage ("Code Compilation") {
            switch ("${javaVersion}") {
                case "Java-8":
                    tool name: 'JAVA8', type: 'jdk'
                    dir (bitBucketRepo) {
                        sh """
                            mvn clean test
                            mvn clean install
                        """
                    }
    	    		break
                case "Java-10":
                    tool name: 'JAVA10', type: 'jdk'
                	dir (bitBucketRepo) {
                        sh """
                            mvn clean test
                            mvn clean install
                        """
                    }
    	    		break
                case "Java-11":
                    dir (bitBucketRepo) {
                        sh """
                            export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                            mvn clean test
                            mvn clean install
                        """
                    }
    	    		break
                case "Core-Java-11":
                    dir (bitBucketRepo) {
                        sh """
                            export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                            mvn clean test
                            mvn clean install assembly:single
                        """
                    }
                    break
                default:
                    echo "No Java Version passed"
                    break
            }
        }
    } catch (e) {
        println e
        if (statusCheck == 'true') {
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Code Compilation failed")
        }
        throw e
    }
}

def reactCodeCompile() {
    try {
        stage("Code Compilation") {
            dir (bitBucketRepo) {
                sh"""
                    npm install
                    CI=true npm run test
                    npm run build DXP
                """
            }
        }
    } catch (e) {
        println e
        if (statusCheck == 'true') {
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Code Compilation failed")
        }
        throw e
    }
}

def sonarAnalysis(def technology, def compiledProjectName, def slnName) {
    try {
        stage ("Sonar Analysis") {
            withSonarQubeEnv('SonarServer') {
                switch ("${technology}") {
                    case "JAVA":
                        dir (bitBucketRepo) {
                            sh"mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.7.0.1746:sonar"
                        }
                        break
                    case "Node":
                        dir (bitBucketRepo) {
                            sh"sonar-scanner"
                        }
                        break
                    case "DotNet":
                        dir (bitBucketRepo) {
                            /*sh"""
                            dotnet-sonarscanner begin /k:"${compiledProjectName}"
                            dotnet build ${slnName}
                            dotnet-sonarscanner end"""*/
                            sh """
                                dotnet test ${compiledProjectName}.Tests/${compiledProjectName}.Tests.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=$WORKSPACE/
                                dotnet-sonarscanner begin /k:"${compiledProjectName}" /d:sonar.cs.opencover.reportsPaths=$WORKSPACE/coverage.opencover.xml
                                dotnet build ${slnName}
                                dotnet-sonarscanner end
                            """
                        }
                        break
                    default: 
                        echo "No Technology passed for Scanning Sonar"
    	    		    break
                }
            }
        }
    } catch (e) {
        println e
        if (statusCheck == 'true') {
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Sonar Analysis failed")
        }
        throw e
    }
}

def qualityGate() {
    stage ("Quality Gate") {
        try {
            timeout(time: 5, unit: 'MINUTES') {
                def count =0
                while (count < 5) {
                    try {
                        def qualityGate = waitForQualityGate()
                        if (qualityGate.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qualityGate.status}"
                        }
                        break
                    } catch (IllegalStateException ise) {
                        count++
                        print ise
                        print "retrying quality gate status"
                    }
                }
            }
        } catch(e) {
            println e
                error("Quality Gate failed")
        }
    }
}

def pullRequestQualityGate() {
    stage ("Quality Gate") {
        try {
            timeout(time: 5, unit: 'MINUTES') {
                def count =0
                while (count < 5) {
                    try {
                        def qg = waitForQualityGate()
                        if (qg.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                            bitbucketBuildStatusNotify("Build","FAILED")
                        }
                        break
                    } catch (IllegalStateException ise) {
                        count++
                        print ise
                        print "retrying quality gate status"
                    }
                }
            }
        } catch(e) {
            println e
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Quality Gate failed")
        }
    }
}

def jarMoveProcess(def isAdapterEnabled) {
    if ("${isAdapterEnabled}" == 'YES' && bitBucketRepo != 'null') {
        sh "mv $WORKSPACE/"+bitBucketRepo+"/"+bitBucketRepo+"/target/*.jar $WORKSPACE/"+bitBucketRepo+"/"
    } else {
        sh "mv $WORKSPACE/"+bitBucketRepo+"/target/*.jar $WORKSPACE/"+bitBucketRepo+"/"
    }
}

def dockerImageProcess(def imageName, def version) {
    try {
        sh'sudo docker login gcr.io/eng-genius-224716'
        stage ("Build Docker Image and Push") {
            docker.withRegistry("http://$DECURTIS_REGISTRY_PATH", "DECURTIS-REGISTRY-CREDENTIALS") {
                def dockerImage = docker.build("$DECURTIS_REGISTRY_PATH/${imageName}:${version}","$WORKSPACE/"+bitBucketRepo+"/")
            }
                sh """
                    sudo docker login gcr.io/dxp-developer-env/
                    docker tag $DECURTIS_REGISTRY_PATH/${imageName}:${version} $DEVELOPER_REGISTRY_PATH/${imageName}:${version}
                    docker push $DEVELOPER_REGISTRY_PATH/${imageName}:${version}
                """
        }
    } catch (e) {
        println e
        if (statusCheck == 'true') {
            bitbucketBuildStatusNotify("Build","FAILED")
            error("Docker build failed")
        }
        throw e
    }
}

def triggerRemoteBuild (buildName, remoteServer, params, block = true) {
    def handle = triggerRemoteJob (
        remoteJenkinsName: remoteServer,
        job: buildName,
        blockBuildUntilComplete: block.toBoolean(),
        parameters: params,
        enhancedLogging: false,
    )
    def status = handle.getBuildStatus().toString()
    def buildUrl = handle.getBuildUrl().toString()
    echo buildUrl + " finished with " + status

    // Read Test Data From Remote Build
    testPlan = null
    if (params) {
        if (params.contains("testRails=true")) {
            testData = handle.readJsonFileFromBuildArchive('test_data.json')
            if (testData.containsKey('test_plan')) {
                testPlan = testData['test_plan']
            }
        }
    }

    if (status in ['SUCCESS', 'FINISHED', 'RUNNING']) {
        return [testPlan, true, buildUrl]
    } else {
        return [testPlan, false, buildUrl]
    }
}

def getBitbucketToken() {
    sh'curl -X POST -u "M5Pz4S2VDJK7c9fR8E:vxGjjGTTBv9Mm3jwH5GuMzZ2AKttAn9G" https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials > bitbucket_token.json'
    tokenJson = readJSON file: 'bitbucket_token.json'
    token = tokenJson.access_token
}

def get_slack_user_id_from_email(emailId) {
    withCredentials([string(credentialsId: 'slack-api-token', variable: 'SLACK_TOKEN')]) {
        def url = "https://slack.com/api/users.lookupByEmail?email=${emailId}"
        def response = httpRequest ignoreSslErrors: true,
            httpMode: 'GET',
            contentType: 'APPLICATION_JSON',
            quiet: true,
            customHeaders: [[name: 'Authorization', value: "Bearer ${SLACK_TOKEN}"]],
            url: url
        def respContent = readJSON text: response.content
        def userId = respContent['user']['id']
        return "${userId}"
    }
}

def bitbucketBuildStatusNotify(def statusKey, def status) {
    try {
        status_url = "https://api.bitbucket.org/2.0/repositories/decurtis/"+bitBucketRepo+"/commit/"+"${commitId}".trim()+"/statuses/build"
        def message
        def jenkinsJobUrl = "https://infra.dxp-decurtis.com/jenkins/job/${jobName}/${buildNumber}/console"
        println statusKey
        println status
        println bitBucketRepo
        switch ("${status}") {
            case "INPROGRESS":
                message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is InProgress.", "url":"'+jenkinsJobUrl+'"}'
                break
            case "SUCCESSFUL":
                message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is Successful.", "url":"'+jenkinsJobUrl+'"}'
                break
            case "FAILED":
                message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is failed.", "url":"'+jenkinsJobUrl+'"}'
                break
            default:
                break
        }
        def post = new URL(status_url).openConnection();
        post.setRequestMethod("POST")
        post.setDoOutput(true)
        post.setRequestProperty("Authorization", "Bearer "+token);
        post.setRequestProperty("Content-Type", "application/json")
        post.getOutputStream().write(message.getBytes("UTF-8"));
        def response = post.getResponseCode();
        if (response != 201 && response != 200) {
            print('An error has occurred.')
            print(response)
        }
    } catch(e) {
        print(e)
        print('An error has occurred ')
    }
}

return this