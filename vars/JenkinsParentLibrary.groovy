#!/bin/groovy
package dxp.decurtis;
import groovy.xml.MarkupBuilder
properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5'))])
def oldVersion
def newVersion
def commitId
def scmVars
def repoName
def jobName
def branchName
def buildNumber
def token
def branch
def technology
    def javaCICDProcess(def isAdapterEnabled,def bitBucketRepoName, def helmName, def branch,def javaVersion, def imageName, def deploymentEnvironment, def dbName)
    {
        repoName= bitBucketRepoName
        technology = "Java"
        deleteDirectory()
        codeCheckout()
        branch=branchName
        javaUnitTest(bitBucketRepoName)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branch)
        javaCodeCompile(javaVersion,bitBucketRepoName)
        jarMoveProcess(isAdapterEnabled,bitBucketRepoName) 
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        qualityGate(bitBucketRepoName) 
        dockerImageProcess(imageName, bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)    
        dxpDatabaseBuild(dbName, bitBucketRepoName, branch)     
        //dxpDatabaseDeployment(dbName,deploymentEnvironment,bitBucketRepoName)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName) 
        bitbucketTagging(bitBucketRepoName,branch)

    }
    def javaCICDProcess(def isAdapterEnabled,def bitBucketRepoName, def helmName, def javaVersion, def imageName, def deploymentEnvironment, def dbName)
    {
        repoName= bitBucketRepoName
        technology = "Java"
        deleteDirectory()
        codeCheckout()
        branch=branchName
        javaUnitTest(bitBucketRepoName)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branchName)
        javaCodeCompile(javaVersion,bitBucketRepoName)
        jarMoveProcess(isAdapterEnabled,bitBucketRepoName) 
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        qualityGate(bitBucketRepoName) 
        dockerImageProcess(imageName, bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)    
        dxpDatabaseBuild(dbName, bitBucketRepoName, branchName)     
        //dxpDatabaseDeployment(dbName,deploymentEnvironment,bitBucketRepoName)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName) 
        bitbucketTagging(bitBucketRepoName,branchName)
    }
    def javaCommonLibCICDProcess(def isAdapterEnabled,def bitBucketRepoName, def helmName,def branch,def javaVersion)
    {
        repoName= bitBucketRepoName
        technology = "Java"
        deleteDirectory()
        codeCheckout()
        javaUnitTest(bitBucketRepoName)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branch)
        javaCodeCompile(javaVersion,bitBucketRepoName)
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        compileJavaCommonLibrary(javaVersion, bitBucketRepoName)
        bitbucketTagging(bitBucketRepoName,branch)
    }
    def nodeCICDProcess(def bitBucketRepoName, def helmName, def technology = "Node", def branch, def imageName, def deploymentEnvironment)
    {
        repoName= bitBucketRepoName
        isAdapterEnabled = null
        technology = "Node"
        deleteDirectory()
        codeCheckout()
        nodeCodeCompile(bitBucketRepoName)
        nodeUnitTest(bitBucketRepoName)
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        qualityGate(bitBucketRepoName) 
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branch)
        dockerImageProcess(imageName,bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName) 
        bitbucketTagging(bitBucketRepoName,branch)
    }
    def nodeCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment)
    {
        repoName= bitBucketRepoName
        isAdapterEnabled = null
        technology = "Node"
        deleteDirectory()
        codeCheckout()
        nodeCodeCompile(bitBucketRepoName)
        nodeUnitTest(bitBucketRepoName)
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        qualityGate(bitBucketRepoName) 
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branchName)
        dockerImageProcess(imageName,bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName) 
        bitbucketTagging(bitBucketRepoName,branchName)
    }
    def reactCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment)
    {
        
        repoName= bitBucketRepoName
        isAdapterEnabled = null
        technology = "React"
        deleteDirectory()
        codeCheckout()
        nodeCodeCompile(bitBucketRepoName)
        nodeUnitTest(bitBucketRepoName)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branchName)
        reactCodeCompile(bitBucketRepoName)
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        qualityGate(bitBucketRepoName) 
        dockerImageProcess(imageName,bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName) 
        bitbucketTagging(bitBucketRepoName,branchName)
    }
    def reactCICDProcessVV(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment)
    {
        
        repoName= bitBucketRepoName
        isAdapterEnabled = null
        technology = "React"
        deleteDirectory()
        codeCheckout()
        nodeCodeCompile(bitBucketRepoName)
        nodeUnitTest(bitBucketRepoName)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branchName)
        reactCodeCompileVV(bitBucketRepoName)
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        qualityGate(bitBucketRepoName) 
        dockerImageProcess(imageName,bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName) 
        bitbucketTagging(bitBucketRepoName,branchName)
    }
    def nodeCommonLibCICDProcess(def bitBucketRepoName, def branch)
    {
        repoName= bitBucketRepoName
        helmName = null
        compiledProjectName = null
        slnName = null
        isAdapterEnabled = null 
        technology = "Node"
        deleteDirectory()
        codeCheckout()
        sonarAnalysis(technology,bitBucketRepoName, null, null)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branch)
        nodeCommonLibCodeCompile(bitBucketRepoName)
        bitbucketTagging(bitBucketRepoName,branch)
    }
    def dotNetCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def dbName, def compiledProjectName, def slnName)
    {
        repoName= bitBucketRepoName
        technology = "DotNet"
        isAdapterEnabled = "NO"
        deleteDirectory()
        codeCheckout()
        dotNetUnitTest(bitBucketRepoName)
        versionUpgrade(isAdapterEnabled,bitBucketRepoName,helmName,technology,branchName)
        dotNetCodeComplie(compiledProjectName,bitBucketRepoName)
//sonarAnalysis(technology,bitBucketRepoName,compiledProjectName,slnName)
//qualityGate(bitBucketRepoName) 
        dockerImageProcess(imageName,bitBucketRepoName)
        helmProcess(helmName,imageName,deploymentEnvironment,bitBucketRepoName)    
        //dxpDatabaseBuild(dbName, bitBucketRepoName,branchName,bitBucketRepoName)   
        //dxpDatabaseDeployment(dbName,deploymentEnvironment)
        //dxpDeployment(helmName,deploymentEnvironment,bitBucketRepoName)
        bitbucketTagging(bitBucketRepoName,branchName)
    }
    def deleteDirectory()
    {
        stage ("CleanUp Workspace") 
        {  
            sh "sudo rm -rf $WORKSPACE/.scannerwork || true"
            deleteDir()
        }
    }
    def codeCheckout() 
    {   stage ("Code CheckOut") 
        {   
            getBitbucketToken()
            scmVars= checkout scm
            commitId = sh(returnStdout: true, script: 'git rev-parse HEAD')
            buildNumber = BUILD_NUMBER
            jobName = JOB_NAME 
            branchName=sh(returnStdout: true, script: 'git symbolic-ref -q --short HEAD || git name-rev --name-only HEAD | sed "s|.*/||g"')

        }
    }
    def dotNetCodeComplie(def compiledProjectName, def bitBucketRepoName)
    {
        stage("Build Process")
        {
            try
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")
                sh"""
                dotnet restore ${compiledProjectName}/${compiledProjectName}.csproj
                dotnet build ${compiledProjectName}/${compiledProjectName}.csproj -c Release
                dotnet publish ${compiledProjectName}/${compiledProjectName}.csproj -c Release -o $WORKSPACE/publish/
                """
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
        }
    }
    def nodeCodeCompile(def bitBucketRepoName)
    {
        stage("Build Process")
        {
            try 
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")
                sh"npm install"
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
        }
    }
    def getBitbucketToken()
    {
        sh'curl -X POST -u "M5Pz4S2VDJK7c9fR8E:vxGjjGTTBv9Mm3jwH5GuMzZ2AKttAn9G" https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials > bitbucket_token.json' 
        tokenJson = readJSON file: 'bitbucket_token.json'
        token = tokenJson.access_token    
    }
    def nodeCommonLibCodeCompile(def bitBucketRepoName)
    {
        stage("Build Process")
        {
            try 
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")
                sh"npm publish"
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
        }
    }
    def reactCodeCompile(def bitBucketRepoName)
    {
        stage("Build Process")
        {
            try 
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")
                sh"npm run build DXP"
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
        }
    }
    def reactCodeCompileVV(def bitBucketRepoName)
    {
        stage("Build Process")
        {
            try 
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")
                sh"npm run build"
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
        }
    }
    def dotNetUnitTest(def bitBucketRepoName)
    {
        stage("Unit Testing")
        {
            try 
            {
                bitbucketBuildStatusNotify("Unit Testing","INPROGRESS", "${bitBucketRepoName}")
                bitbucketBuildStatusNotify("Unit Testing","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Unit Testing","FAILED", "${bitBucketRepoName}")
                error("Unit Testing failed")
            }
        }
    }
    def nodeUnitTest(def bitBucketRepoName)
    {
        stage("Unit Testing")
        {
            try 
            {
                bitbucketBuildStatusNotify("Unit Testing","INPROGRESS", "${bitBucketRepoName}")
                sh"npm run test"
                bitbucketBuildStatusNotify("Unit Testing","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Unit Testing","FAILED", "${bitBucketRepoName}")
                error("Unit Testing failed")
            }
        }
    }
    def javaUnitTest(def bitBucketRepoName) 
    {    
        stage("Unit Testing")
        {
            try 
            {
                bitbucketBuildStatusNotify("Unit Testing","INPROGRESS", "${bitBucketRepoName}")
                echo "Dummy unit test"
                bitbucketBuildStatusNotify("Unit Testing","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Unit Testing","FAILED", "${bitBucketRepoName}")
                error("Unit Testing failed")
            }
        }
    }   
    def versionUpgrade(def isAdapterEnabled, def bitBucketRepoName, def helmName, def technology, def branch="master") 
    {   
        stage("Version Update")
        {    
            try 
            {
                bitbucketBuildStatusNotify("Version Update","INPROGRESS", "${bitBucketRepoName}")
                switch("${technology}") 
                {
                     case "Java":
                        oldVersion = readMavenPom().getVersion()
                        oldPOMVersion = getPomVersion("${isAdapterEnabled}", "${bitBucketRepoName}")
                        def BUILD_NUMB  = Integer.parseInt(oldPOMVersion[4]) + 1;
                        newVersion = "${oldPOMVersion[1]}.${oldPOMVersion[2]}.${oldPOMVersion[3]}-${BUILD_NUMB}"
                        if("${isAdapterEnabled}" == 'YES')
                        {
                            sh"""
                            cd ${bitBucketRepoName}
                            mvn -B versions:set -DgenerateBackupPoms=false -DnewVersion=${newVersion}"""
                        }
                        
                        sh"mvn -B versions:set -DgenerateBackupPoms=false -DnewVersion=${newVersion}"
                        versionUpgradeInternal(bitBucketRepoName,branch,technology)
                        break
                    case "Node":
                    case "React":
                        oldVersionJson = readJSON file: 'package.json'
                        oldVersion = oldVersionJson.version
                        sh "npm --no-git-tag-version version patch"
                        newVersionJson = readJSON file: 'package.json'
                        newVersion = newVersionJson.version
                        versionUpgradeInternal(bitBucketRepoName,branch,technology)
                        break
                    case "DotNet":
                        sh'''
                        vars1=$(cat Directory.Build.props | grep "<Version>" | cut -d"<" -f2 | cut -d">" -f2)
                        vars2=$(echo $vars1 | awk -F"." '{print $1 "." $2 "."}')
                        vars3=$(cat Directory.Build.props | grep "<Version>" | cut -d"<" -f2 | cut -d">" -f2 | cut -d"." -f3)
                        vars4=$(echo $vars3 | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}')
                        vars5=$(echo $vars2$vars4)
                        sed -i 's|'"$vars1"'|'"$vars5"'|g' Directory.Build.props
                        '''
                        newVersion = sh(script: 'cat Directory.Build.props | grep "<Version>" | cut -d"<" -f2 | cut -d">" -f2', returnStdout: true)
                        newVersion = newVersion.trim()
                        versionUpgradeInternal(bitBucketRepoName,branch,technology)
                        break
                    default:
                        break
                }
                bitbucketBuildStatusNotify("Version Update","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Version Update","FAILED", "${bitBucketRepoName}")
                error("Version Update failed")
            }
        }
    }
    def javaCodeCompile(def javaVersion, def bitBucketRepoName)
    {
        stage ("Build Process") 
        {
            try 
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")
                switch("${javaVersion}") 
                {
                     case "Java-8":
                	    tool name: 'JAVA8', type: 'jdk'
                        sh"mvn clean install"
    		  	    break
  			        case "Java-10":
                        tool name: 'JAVA10', type: 'jdk'
                        sh"mvn clean install"
    		  	    break
  			        case "Java-11":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean install"""
    		  	    break
                     case "Java-11-sync":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean install assembly:single"""
  			        default:
                        echo "No Java Version passed"
    		  	    break
    	        }
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
              
        }
    }
    def compileJavaCommonLibrary(def javaVersion, def bitBucketRepoName) 
    {
        stage ("Build Process") 
        {
            try 
            {
                bitbucketBuildStatusNotify("Build Process","INPROGRESS", "${bitBucketRepoName}")   
                switch("${javaVersion}") 
                {
                     case "Java-8":
                	    tool name: 'JAVA8', type: 'jdk'
                        sh"mvn clean deploy"
    		  	    break
  			        case "Java-10":
                        tool name: 'JAVA10', type: 'jdk'
                        sh"mvn clean deploy"
    		  	    break
  			        case "Java-11":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean deploy"""
    		  	    break
                     case "Java-11-sync":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean install assembly:single"""
  			        default:
                        echo "No Java Version passed"
    		  	    break
    	        }
                bitbucketBuildStatusNotify("Build Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Process","FAILED", "${bitBucketRepoName}")
                error("Build Process failed")
            }
        }
    }
    def sonarAnalysis(def technology, def bitBucketRepoName, def compiledProjectName, def slnName) 
    {
        stage ("Sonar Analysis")
        {
            try 
            {
                bitbucketBuildStatusNotify("Sonar Analysis","INPROGRESS", "${bitBucketRepoName}")        
                withSonarQubeEnv('SonarServer') 
                {   
                    switch("${technology}") 
                    {
                         case "Java":
                         sh"mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.7.0.1746:sonar"
                         break
                         case "Node":
                         sh"sonar-scanner"
                         break
                         case "DotNet":
                         sh'''
                            dotnet test ${compiledProjectName}.Tests/${compiledProjectName}.Tests.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=$WORKSPACE/
                            dotnet build-server shutdown
                            dotnet-sonarscanner begin /k:"${compiledProjectName}" /d:sonar.cs.opencover.reportsPaths=$WORKSPACE/coverage.opencover.xml
                            dotnet build ${slnName}
                            dotnet-sonarscanner end
                            '''
                        break
                    default: 
                        echo "No Technology passed for Scanning Sonar"
    		  	    break
                    }    
                }
                bitbucketBuildStatusNotify("Sonar Analysis","SUCCESSFUL", "${bitBucketRepoName}")   
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Sonar Analysis","FAILED", "${bitBucketRepoName}")   
                error("Sonar Analysis failed")
            }
        }
    }
    def qualityGate(def bitBucketRepoName) 
    {
        stage ("Quality Gate")
        {
            try 
            {
                bitbucketBuildStatusNotify("Quality Gate","INPROGRESS", "${bitBucketRepoName}")        
                /*timeout(time: 20, unit: 'MINUTES') 
                { 
                     waitForQualityGate abortPipeline: true
                     def qg = waitForQualityGate()
                     if (qg.status != 'OK') 
                     {
                       error "Pipeline aborted due to quality gate failure: ${qg.status}"
                     }
                }*/
                bitbucketBuildStatusNotify("Quality Gate","SUCCESSFUL", "${bitBucketRepoName}")        
            }
            catch(e) 
            {
                 bitbucketBuildStatusNotify("Quality Gate","FAILED", "${bitBucketRepoName}")   
                 error("Quality Gate failed")    
            }
        }
    }
    /*
        This method is required to move compiled Jar to root directory. Parameters
        isAdapterEnabled = If Component is build with Adapters
        bitBucketRepoName = IF Adapter is enabled then this value should not be null
    */
    def jarMoveProcess(def isAdapterEnabled, def bitBucketRepoName) 
    {
        if ("${isAdapterEnabled}" == 'YES' && "${bitBucketRepoName}" != null)
        { 
            sh "mv $WORKSPACE/${bitBucketRepoName}/target/*.jar $WORKSPACE/"
        }
        else
        {   
            sh "mv $WORKSPACE/target/*.jar $WORKSPACE/"            
        }
    }
    def dockerImageProcess(def imageName, def bitBucketRepoName)
    {
        stage ("Build Docker Image and Push")
        {
            try
            {   
                bitbucketBuildStatusNotify("Build Docker Image and Push","INPROGRESS", "${bitBucketRepoName}")
                image="$DECURTIS_REGISTRY_PATH/${imageName}:${newVersion}".trim()
                docker.withRegistry("https://"+image, "DECURTIS-REGISTRY-CREDENTIALS") 
                {
                    def dockerImage = docker.build(image,"$WORKSPACE/")   
                    dockerImage.push()
                }
                sh""" 
                    docker tag ${image} $NCL_REGISTRY_PATH/${imageName}:${newVersion}
                    docker tag ${image} $VV_REGISTRY_PATH/${imageName}:${newVersion} 
                """
                docker.withRegistry("http://$NCL_REGISTRY_PATH", "NCL-REGISTRY-CREDENTIALS") 
                {
                    sh "docker push $NCL_REGISTRY_PATH/${imageName}:${newVersion}"
                }
                docker.withRegistry("http://$VV_REGISTRY_PATH", "VV-REGISTRY-CREDENTIALS") 
                {
                    sh "docker push $VV_REGISTRY_PATH/${imageName}:${newVersion}"
                }
                bitbucketBuildStatusNotify("Build Docker Image and Push","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Build Docker Image and Push","FAILED", "${bitBucketRepoName}")
                error("Docker Image and Push failed")  

            }
        }
    }
    def helmProcess(def helmName, def imageName, def deploymentEnvironment, def bitBucketRepoName)
    {
        stage ("Helm Process")
        {
            try 
            {
                bitbucketBuildStatusNotify("Helm Process","INPROGRESS", "${bitBucketRepoName}")
                if("${helmName}" !=  'null')
                {
                    checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-helm-charts'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: '876380b7-0ac4-4d27-b615-e263a6c143a0', url: "https://bitbucket.org/decurtis/dxp-helm-charts.git"]]] 
                    dir("$WORKSPACE/dxp-helm-charts/dxp-components")
                    {
                        def chartYaml = readYaml file: "${helmName}/Chart.yaml"
                        chartYaml.version = "${newVersion}".trim()
                        sh "rm ${helmName}/Chart.yaml"
                        writeYaml file: "${helmName}/Chart.yaml", data: chartYaml
                        
                        sh"""
                        git add .
                        git commit -m "DCP-1234 Updating ${helmName} Helm Chart version number"
                        git push https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/dxp-helm-charts.git HEAD:master --tags
                        helm lint "${helmName}/"
                        helm package ${helmName}
                        helm gcs init gs://dxp-helm-charts/${helmName}/
                        helm repo add gcs-helm-${helmName} gs://dxp-helm-charts/${helmName}
                        helm gcs push ${helmName}-${newVersion}.tgz gs://dxp-helm-charts/${helmName}
                        """
                    }
                    checkout scm: [$class: 'GitSCM', branches: [[name: 'dev']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-helm-values'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: '876380b7-0ac4-4d27-b615-e263a6c143a0', url: "https://bitbucket.org/decurtis/dxp-helm-values.git"]]]   
                    dir("$WORKSPACE/dxp-helm-values")
                    {
                        switch("${deploymentEnvironment}")
                        {
                            case "ALL":
                                updateComponentVersionInShore(helmName, imageName)
                                updateComponentVersionInShip(helmName, imageName)
                                break
                            case "Shore":
                                updateComponentVersionInShore(helmName, imageName)
                                break                    
                            case "Ship": 
                                updateComponentVersionInShip(helmName, imageName)
                                break
                        }
                    
                        sh""" 
                        git add --all
                        git commit -m "DCP-1234 Updating version number for CI"
                        git push https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/dxp-helm-values.git HEAD:dev
                        """
                     }
                }
                bitbucketBuildStatusNotify("Helm Process","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e)
            {
                bitbucketBuildStatusNotify("Helm Process","FAILED", "${bitBucketRepoName}")
                error("Docker Image and Push failed")
            }
        }
    }
    def dxpDatabaseBuild(def dbName, def bitBucketRepoName, def branch)
    {    
        stage ("Database Scripts Build")
        {
            try 
            {
                if("${dbName}" != 'null')
                {
                    println dbName 
                    bitbucketBuildStatusNotify("Database Scripts Build","INPROGRESS", "${bitBucketRepoName}")
                    //dbVersionUpdate("${dbName}", "${bitBucketRepoName}", "${branch}")
                    sh"""
                    tar -zcvf ${dbName}-${newVersion}.tar.gz ${dbName}
                    gsutil cp  ${dbName}-${newVersion}.tar.gz gs://dxp-databases-scripts/${dbName}/
                    """
                    bitbucketBuildStatusNotify("Database Scripts Build","SUCCESSFUL", "${bitBucketRepoName}")
                }
            }
            catch(e) 
            {
                bitbucketBuildStatusNotify("Database Scripts Build","FAILED", "${bitBucketRepoName}")
                error("Database Scripts Build")
            }
        }
    }
    def dxpDatabaseDeployment(def dbName, def deploymentEnvironment, def bitBucketRepoName)
    {
        stage("Database Deployment")
        {
            try
            {
                bitbucketBuildStatusNotify("Database Deployment","INPROGRESS", "${bitBucketRepoName}")
                sh"""
                mkdir -p $WORKSPACE/dxp-databases-scripts/${dbName} 
                gsutil -m cp -R gs://dxp-databases-scripts/${dbName}/${dbName}-${newVersion}.tar.gz  $WORKSPACE/dxp-databases-scripts/${dbName}/
                tar xvzf $WORKSPACE/dxp-databases-scripts/${dbName}/${dbName}-${newVersion}.tar.gz   $WORKSPACE/dxp-databases-scripts/${dbName}/                                 
                chmod -R u+rwx $WORKSPACE/dxp-databases-scripts/${dbName}/
                """ 
                switch("${deploymentEnvironment}")
                {   
                    case "ALL":
                    sh"""
                    PGHOST=postgresql.dev-shore.dxp-decurtis.com  PGPORT="5432" PGUSER=dxp PGPASSWORD=S4DUBD*f4k7MXJy#o#68   $WORKSPACE/dxp-databases-scripts/${dbName}/Create.sh  ;done
                    PGHOST=postgresql.dev-sagar.dxp-decurtis.com PGPORT="5432"  PGUSER=dxp PGPASSWORD=S4DUBD*f4k7MXJy#o#68   $WORKSPACE/dxp-databases-scripts/${dbName}/Create.sh  ;done
                    """
                    break 
                    case "Shore":
                    sh"PGHOST=postgresql.dev-shore.dxp-decurtis.com  PGPORT=5432 PGUSER=dxp PGPASSWORD=S4DUBD*f4k7MXJy#o#68 $WORKSPACE/dxp-databases-scripts/${dbName}/Create.sh  ;done"
                    break
                    case "Ship":
                    sh"""
                    PGHOST=postgresql.dev-sagar.dxp-decurtis.com PGPORT="5432" PGUSER=dxp PGPASSWORD=S4DUBD*f4k7MXJy#o#68 $WORKSPACE/dxp-databases-scripts/${dbName}/Create.sh  ;done
                    """
                    break
                    default: 
                    echo "Environment doesn't exist with configuration"
                    break
                }
                bitbucketBuildStatusNotify("Database Deployment","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(Exception e) 
            {
                bitbucketBuildStatusNotify("Database Deployment","FAILED", "${bitBucketRepoName}")   
                error("Database Deployment failed")
            }    
        }
    }
    def dxpDeployment(def componentName, def environment, def bitBucketRepoName) 
    {
        stage("Component Deployment")
        {
            try 
            {
                bitbucketBuildStatusNotify("Component Deployment","INPROGRESS", "${bitBucketRepoName}")
                if("${componentName}" != 'null')
                {
                    switch("${environment}")
                    {
                        case "ALL":
                        deploymentDXPShip(componentName)
                        deploymentDXPShore(componentName)
                        break 
                        case "Shore":
                        deploymentDXPShore(componentName)
                        break
                        case "Ship":
                        deploymentDXPShip(componentName)
                        break
                        default: 
                        echo "Environment doesn't exist with configuration"
                        break
                    }
                
                    sh"helm repo remove gcs-helm-${componentName}"
                }
                bitbucketBuildStatusNotify("Component Deployment","SUCCESSFUL", "${bitBucketRepoName}")
            }
            catch(e)
            { 
                bitbucketBuildStatusNotify("Component Deployment","FAILED", "${bitBucketRepoName}") 
                error("Component Deployment failed")
            }
        }
    }
    def bitbucketBuildStatusNotify(def statusKey, def status, def bitBucketRepoName)
    {         
        status_url = "https://api.bitbucket.org/2.0/repositories/decurtis/"+"${bitBucketRepoName}"+"/commit/"+"${commitId}".trim()+"/statuses/build"   
        def message
        def jenkinsJobUrl = "https://infra.dxp-decurtis.com/jenkins/job/${jobName}/${buildNumber}/console"
        switch("${status}")
        {
            case "INPROGRESS":
            message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is InProgress.", "url":"'+jenkinsJobUrl+'"}'
            break
            case "SUCCESSFUL":
            message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is Successful.", "url":"'+jenkinsJobUrl+'"}'
            break
            case "FAILED":
            message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is failed.", "url":"'+jenkinsJobUrl+'"}'
            break
            default:
            break
        }
        def post = new URL(status_url).openConnection();
        post.setRequestMethod("POST")
        post.setDoOutput(true)
        post.setRequestProperty("Authorization", "Bearer "+token);
        post.setRequestProperty("Content-Type", "application/json")
        post.getOutputStream().write(message.getBytes("UTF-8"));
        def response = post.getResponseCode();
        if(response.equals(201) || response.equals(200) ) 
        {
            print('BitBucket Notified Successfully!')
        }   
        else
        {
            print('An error has occurred while notifying to BitBucket.') 
            print(response)
            //print(post)
        }
    }
    def bitbucketTagging(def bitBucketRepoName,def branchName)
    {
        try 
        {
            stage("BitBucket Version Tagging")
            {
                bitbucketBuildStatusNotify("Version Tagging","INPROGRESS", bitBucketRepoName)
                sh"""
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push  --force --follow-tags https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/${bitBucketRepoName}.git HEAD:${branchName}
                """
                bitbucketBuildStatusNotify("Version Tagging","SUCCESSFUL", bitBucketRepoName)
            } 
        }
        catch(e) 
        {
            bitbucketBuildStatusNotify("Version Tagging","FAILED", bitBucketRepoName)
            error("BitBucket Version Tagging failed")
        }
        
    }
    def mailNotification(def email)
    {

    }
    def slackNotification()
    {
        
    }
    def slackFailureNotification()
    {
        bitbucketBuildStatusNotify("Something","FAILED", "${repoName}")
    }
    /* Internal Methods (Not for external use)*/
    def getPomVersion(def isAdapterEnabled, def bitBucketRepoName) 
    {
        def matcher
        echo "${bitBucketRepoName}"
        if("${isAdapterEnabled}" == 'YES')
        {    
            matcher = readFile("$WORKSPACE/${bitBucketRepoName}/pom.xml") =~ '<version>(\\d*)\\.(\\d*)\\.(\\d*)-(\\d*)</version>'
        }
        else
        {
            matcher = readFile('pom.xml') =~ '<version>(\\d*)\\.(\\d*)\\.(\\d*)-(\\d*)</version>'
        }
        
        matcher ? matcher[0] : null
    }
    def dbVersionUpdate(def dbName, def bitBucketRepoName, def branch)
    {   
        stage ("Database Version Upgrade")
        {
            dir("$WORKSPACE/${dbName}/UpgradeScripts/Scripts")
            {
                def sql= readFile "UpdateComponentVersion.sql"
                def vars1 = sql[0..<sql.indexOf("BEGIN")].split("\n").findAll {it.startsWith("DECLARE")}.findAll{it.contains('current_version')}.toString()    
                def version1= vars1.split(":=")
                def prevVersion = version1[1].replaceAll("'", "").replaceAll(";", "").replaceAll("]", "").trim().toString()
                echo "${prevVersion}"
                sh"""sed -i 's|${prevVersion}|${newVersion}|g' UpdateComponentVersion.sql
                git add --all
                git commit -m "DCP-1234 Updating version number for CI"
                git push https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/${bitBucketRepoName}.git HEAD:${branch}
                """
            }
        }
    }
    def versionUpgradeInternal(def bitBucketRepoName, def branch, def technology) 
    {
        switch(technology)
        {
            case "Java":
             sh"""
             git add .
             git commit -m "DCP-1234 Updating version ${newVersion}"
             git push https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/${bitBucketRepoName}.git HEAD:${branch}
             """
            break
            case "Node":
             sh"""
             git add .
             git commit -m "DCP-1234 Updating version ${newVersion}"
             git push https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/${bitBucketRepoName}.git HEAD:${branch}"""            
            break
            case "DotNet":
             sh"""
             git add .
             git commit -m "DCP-1234 Updating version ${newVersion}"
             git push https://ciduser:${CIDUSER_PASSWORD}@bitbucket.org/decurtis/${bitBucketRepoName}.git HEAD:${branch}"""              
            break
            default:
            break
        }
       
    }
    def updateComponentVersionInShore(def helmName, def imageName)
    {
        def shoreValues = readYaml file: "values_shore.yaml"
        shoreValues.global.imageVersion."${helmName}" = "${imageName}:${newVersion}"
        sh "rm values_shore.yaml"
        writeYaml file: "values_shore.yaml", data: shoreValues
    }
    def updateComponentVersionInShip(def helmName, def imageName)
    {
        def sagarValues = readYaml file: "values_sagar.yaml"
        sagarValues.global.imageVersion."${helmName}" = "${imageName}:${newVersion}"
        sh "rm values_sagar.yaml"
        writeYaml file: "values_sagar.yaml", data: sagarValues
       
    }
    def deploymentDXPShip(def componentName)
    {
        sh"""
        export KUBECONFIG=/home/jenkins/DXP/dxp-gcp-dev.conf
        helm repo update gcs-helm-${componentName}
        helm upgrade --install  ${componentName}-dev-sagar gcs-helm-${componentName}/${componentName} --values $WORKSPACE/dxp-helm-values/values_sagar.yaml --version ${newVersion} --namespace sagar 
        """
    }
    def deploymentDXPShore(def componentName)
    {
        sh"""
        export KUBECONFIG=/home/jenkins/DXP/dxp-gcp-dev.conf
        helm repo update gcs-helm-${componentName}
        helm upgrade --install ${componentName}-dev-shore gcs-helm-${componentName}/${componentName} --values $WORKSPACE/dxp-helm-values/values_shore.yaml --version ${newVersion} --namespace shore
        """
    }
return this