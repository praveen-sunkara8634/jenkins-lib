#!/bin/groovy
package dxp.decurtis;
import groovy.xml.MarkupBuilder
properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5'))])
def oldVersion
def newVersion
def commitId
def scmVars
def repoName
def jobName
def branchName
def buildNumber
def token
def branch
def technology
def authorEmail
def reviewerEmail
def deploymentStatus
def environments
def newenv
def fileshore
def filesagar
def commitmessage
def jiraId
def assignee
def starttime
def endtime
def startBuild
def endBuild

def setEnv(def env) {
    if (!env) {
        newenv="ALL"
    } else{
        newenv = env
    }
    println newenv
    environments = newenv.split(',')
}
def getEndBuildTime(){
    endBuild = sh(returnStdout: true, script: 'date +%s').toInteger()
}
def javaCICDProcess(def isAdapterEnabled,def bitBucketRepoName, def helmName, def javaVersion, def imageName, def deploymentEnvironment, def dbName, def env) {
    repoName= bitBucketRepoName
    technology = "Java"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName, dbName)
    javaCodeCompile(javaVersion)
    jarMoveProcess(isAdapterEnabled)
    dxpDatabaseBuild(dbName)
    sonarAnalysis(technology, null, null)
    qualityGate()
    dockerBuildProcess(imageName,newenv)
    helmProcess(helmName,imageName,deploymentEnvironment,technology)
    getEndBuildTime()
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    stage("Master-CI") {
        if(environments.contains("DXP") || environments.contains("ALL")) {
            try {
                bitbucketBuildStatusNotify("Test","INPROGRESS")
                build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                //dxpDeployment(helmName,deploymentEnvironment)
            } catch(Exception e) {
                bitbucketBuildStatusNotify("Test","FAILED")
                insertCIStatus("Master-CI Failed","FAILED",false)
                error("Master-CI job failed")
            }
            //dxpDatabaseDeployment(dbName,deploymentEnvironment)
            //bitbucketTagging(branchName)
            pushUpgradedHelmVersion(deploymentEnvironment, technology)
            insertIntoPostgres()
            insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
            //postmanScript(helmName,deploymentEnvironment)
            bitbucketBuildStatusNotify("Test","SUCCESSFUL")
        }
    }
}

def javaCommonLibCICDProcess(def isAdapterEnabled,def bitBucketRepoName, def helmName,def javaVersion) {
    repoName= bitBucketRepoName
    technology = "Java"
    codeCheckout()
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName, null)
    javaCodeCompile(javaVersion)
    sonarAnalysis(technology, null, null)
    getEndBuildTime()
    //bitbucketTagging(branchName)
    insertIntoPostgres()
    insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
}

def nodeCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def env) {
    repoName= bitBucketRepoName
    isAdapterEnabled = null
    technology = "Node"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName, null)
    nodeCodeCompile()
    sonarAnalysis(technology, null, null)
    qualityGate()
    dockerBuildProcess(imageName, newenv)
    helmProcess(helmName,imageName,deploymentEnvironment,technology)
    getEndBuildTime()
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    stage("Master-CI") {
        if(environments.contains("DXP") || environments.contains("ALL")) {
            try {
                bitbucketBuildStatusNotify("Test","INPROGRESS")
                build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                //dxpDeployment(helmName,deploymentEnvironment)
            } catch(Exception e) {
                bitbucketBuildStatusNotify("Test","FAILED")
                insertCIStatus("Master-CI Failed","FAILED",false)
                error("Master-CI job failed")
            }
            //bitbucketTagging(branchName)
            pushUpgradedHelmVersion(deploymentEnvironment,technology)
            insertIntoPostgres()
            insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
            //postmanScript(helmName,deploymentEnvironment)
            bitbucketBuildStatusNotify("Test","SUCCESSFUL")
        }
    }
}

def reactCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def env) {
    repoName= bitBucketRepoName
    isAdapterEnabled = null
    technology = "React"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName,null)
    nodeCodeCompile()
    sonarAnalysis(technology,null, null)
    qualityGate()
    reactCodeCompile(newenv,helmName)
    helmProcess(helmName,imageName,deploymentEnvironment,technology)
    getEndBuildTime()
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    stage("Master-CI") {
        if(environments.contains("DXP") || environments.contains("ALL")) {
            try {
                bitbucketBuildStatusNotify("Test","INPROGRESS")
                build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                //dxpDeployment(helmName,deploymentEnvironment)
            } catch(Exception e) {
                bitbucketBuildStatusNotify("Test","FAILED")
                insertCIStatus("Master-CI Failed","FAILED",false)
                error("Master-CI job failed")
            }
            //bitbucketTagging(branchName)
            pushUpgradedHelmVersion(deploymentEnvironment,technology)
            insertIntoPostgres()
            insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
            //postmanScript(helmName,deploymentEnvironment)
            bitbucketBuildStatusNotify("Test","SUCCESSFUL")
        }
    }
}

def nodeCommonLibCICDProcess(def bitBucketRepoName, def branch) {
    repoName= bitBucketRepoName
    helmName = null
    isAdapterEnabled = null
    technology = "Node-Common"
    codeCheckout()
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName,null)
    nodeCommonLibCodeCompile()
    sonarAnalysis(technology,null, null)
    getEndBuildTime()
    //bitbucketTagging(branch)
    insertIntoPostgres()
    insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
}

def androidCICDProcess(def bitBucketRepoName) {
    repoName= bitBucketRepoName
    helmName = null
    isAdapterEnabled = null
    technology = "Android"
    codeCheckout()
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName,null)
    androidCodeCompilePriority()
    androidCodeCompileOthers()
    //sonarAnalysis(technology,null, null)
    getEndBuildTime()
    //bitbucketTagging(branchName)
    insertIntoPostgres()
    insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
}

def dotNetCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def dbName, def compiledProjectName, def slnName, def env) {
    repoName= bitBucketRepoName
    technology = "DotNet"
    isAdapterEnabled = "NO"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName,dbName)
    dotNetCodeCompile(compiledProjectName)
    dxpDatabaseBuild(dbName)
    sonarAnalysis(technology,compiledProjectName,slnName)
    qualityGate()
    dockerBuildProcess(imageName, newenv)
    helmProcess(helmName,imageName,deploymentEnvironment,technology)
    getEndBuildTime()
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    stage("Master-CI") {
        if(environments.contains("DXP") || environments.contains("ALL")) {
            try {
                bitbucketBuildStatusNotify("Test","INPROGRESS")
                build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                //dxpDeployment(helmName,deploymentEnvironment)
            } catch(Exception e) {
                bitbucketBuildStatusNotify("Test","FAILED")
                insertCIStatus("Master-CI Failed","FAILED",false)
                error("Master-CI job failed")
            }
            //dxpDatabaseDeployment(dbName,deploymentEnvironment)
            //bitbucketTagging(branchName)
            pushUpgradedHelmVersion(deploymentEnvironment,technology)
            insertIntoPostgres()
            insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
            //postmanScript(helmName,deploymentEnvironment)
            bitbucketBuildStatusNotify("Test","SUCCESSFUL")
        }
    }
}
def scalaCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def env, def process, def dbName, def moduleName, def appName) {
    repoName= bitBucketRepoName
    isAdapterEnabled = null
    technology = "Scala"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName,dbName)
    buildScala(process,moduleName)
    dxpDatabaseBuild(dbName)
    sonarAnalysis(technology,null, null)
    getEndBuildTime()
    if (helmName) {
        dockerBuildProcess(imageName,newenv)
        helmProcess(helmName,imageName,deploymentEnvironment,technology)
        getEndBuildTime()
        bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
        stage("Master-CI") {
            if(environments.contains("DXP") || environments.contains("ALL")) {
                try {
                    bitbucketBuildStatusNotify("Test","INPROGRESS")
                    build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                    //dxpDeployment(helmName,deploymentEnvironment)
                } catch(Exception e) {
                    bitbucketBuildStatusNotify("Test","FAILED")
                    insertCIStatus("Master-CI Failed","FAILED",false)
                    error("Master-CI job failed")
                }
                //dxpDatabaseDeployment(dbName,deploymentEnvironment)
                //bitbucketTagging(branchName)
                pushUpgradedHelmVersion(deploymentEnvironment,technology)
                insertIntoPostgres()
                insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
                //postmanScript(helmName,deploymentEnvironment)
                bitbucketBuildStatusNotify("Test","SUCCESSFUL")
            }
        }
    } else {
        print "Module is uploaded to artifactory."
        bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    }
    if (appName) {
        buildApp(appName)
    }
}
def pythonCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def env, def dbName) {
    repoName= bitBucketRepoName
    isAdapterEnabled = null
    technology = "Python"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName, dbName)
    //build step
    dxpDatabaseBuild(dbName)
    sonarAnalysis(technology,null, null)
    dockerBuildProcess(imageName, newenv)
    helmProcess(helmName,imageName,deploymentEnvironment,technology)
    getEndBuildTime()
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    stage("Master-CI") {
        if(environments.contains("DXP") || environments.contains("ALL")) {
            try {
                bitbucketBuildStatusNotify("Test","INPROGRESS")
                build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                //dxpDeployment(helmName,deploymentEnvironment)
            } catch(Exception e) {
                bitbucketBuildStatusNotify("Test","FAILED")
                insertCIStatus("Master-CI Failed","FAILED",false)
                error("Master-CI job failed")
            }
            pushUpgradedHelmVersion(deploymentEnvironment,technology)
            insertIntoPostgres()
            insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
            bitbucketBuildStatusNotify("Test","SUCCESSFUL")
        }
    }
}

def nodeMaxiruCICDProcess(def bitBucketRepoName, def helmName, def imageName, def deploymentEnvironment, def env) {
    repoName= bitBucketRepoName
    isAdapterEnabled = null
    technology = "NodeMaxiru"
    codeCheckout()
    setEnv(env)
    bitbucketBuildStatusNotify("Build Process","INPROGRESS")
    versionUpgrade(isAdapterEnabled,helmName,technology,branchName, null)
    nodeCodeCompile()
    dockerBuildProcess(imageName, newenv)
    helmProcess(helmName,imageName,deploymentEnvironment,technology)
    getEndBuildTime()
    bitbucketBuildStatusNotify("Build Process","SUCCESSFUL")
    stage("Master-CI") {
        if(environments.contains("DXP") || environments.contains("ALL")) {
            try {
                bitbucketBuildStatusNotify("Test","INPROGRESS")
                build job: 'Master-CI', parameters: [[$class: 'StringParameterValue', name: 'ParentWorkspace', value: "${WORKSPACE}/${bitBucketRepoName}"],[$class: 'StringParameterValue', name: 'UserName', value: "${authorEmail}"],[$class: 'StringParameterValue', name: 'Repository', value: "${bitBucketRepoName}"]]
                //dxpDeployment(helmName,deploymentEnvironment)
            } catch(Exception e) {
                bitbucketBuildStatusNotify("Test","FAILED")
                insertCIStatus("Master-CI Failed","FAILED",false)
                error("Master-CI job failed")
            }
            //bitbucketTagging(branchName)
            pushUpgradedHelmVersion(deploymentEnvironment,technology)
            insertIntoPostgres()
            insertCIStatus("DXP-CI completed successfully","SUCCESS",false)
            //postmanScript(helmName,deploymentEnvironment)
            bitbucketBuildStatusNotify("Test","SUCCESSFUL")
        }
    }
}

def sendEmail(def componentName) {
    if ("${deploymentStatus}" == 'true' && "${params.Technology}" != 'Android') {
         emailext attachmentsPattern: '',
         body: "Hey ${authorEmail}!\n\n Your ${componentName} successfully deployed on DXP-Dev.\n\nThanks,\n\nTeam Avengers",
         subject: "DXP Dev Deployment",to: "${authorEmail}"
    } else if ("${deploymentStatus}" == 'false') {
          emailext attachmentsPattern: '',
          body: "Hey ${authorEmail}!\n\n ${componentName} is not deployed on DXP-Dev and is rollbacked to previous version.\n\nThanks,\n\nTeam Avengers",
          subject: "DXP Dev Deployment",to: "${authorEmail}"
    } else if("${deploymentStatus}" == 'true' && "${params.Technology}" == 'Android') {
          emailext attachmentsPattern: '',
          body: "Hey ${authorEmail}!\n\n You can find your apks here - https://console.cloud.google.com/storage/browser/dxp-android-builds/android-local/${newVersion}/?project=eng-genius-224716.\n\nThanks,\n\nTeam Avengers",
          subject: "APK Builds",to: "${authorEmail},vertical-qa@decurtis.com"
    }
}

def deleteDirectory() {
    stage ("CleanUp Workspace") {
        sh "sudo rm -rf $WORKSPACE/.scannerwork || true"
        cleanWs()
    }
}

def codeCheckout() {
    stage ("Code CheckOut") {
        try {
            starttime = sh(returnStdout: true, script: 'date +%s').toInteger()
            echo "start time: ${starttime}"
            checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'DXP-Automation'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-automation.git"]]]
            getBitbucketToken()
            buildNumber = BUILD_NUMBER
            jobName = JOB_NAME
            dir ("$WORKSPACE/"+repoName) {
                commitId = sh(returnStdout: true, script: 'git rev-parse HEAD')
                branchName="master"
                sh"""curl -H "Authorization: Bearer ${token}" -H "Accept: application/json" -o file.json https://api.bitbucket.org/2.0/repositories/decurtis/${repoName}/commit/${commitId}"""
                responseJson = readJSON file: "$WORKSPACE/"+repoName+"/file.json"
                if(responseJson != null) {
                    author = responseJson.author.raw.split('<')
                    authorEmail = author[1].split('>')[0]
                    println authorEmail
                    commitmessage = responseJson.message
                    println commitmessage
                    def matchesdcp = commitmessage =~/(DCP-\d*)/
                    def matchessrvc = commitmessage =~/(SRVC-\d*)/
                    if (matchesdcp.size()>0){
                        jiraId = matchesdcp[0][1]
                    } else if(matchessrvc.size()>0){
                        jiraId = matchessrvc[0][1]
                    } else {
                        jiraId="NA"
                        assignee="NA"
                    }
                }
                println "${jiraId}"
                if ("${jiraId}" != "NA") {
                    sh"""curl -X GET -H "Content-Type: application/json" -H "Authorization: Basic cHVuZWV0Lm1hdGh1ckBkZWN1cnRpcy5jb206cmp6M2x3TGlTSTN4djA4Zk5nYkVCMENF" -L http://decurtis.atlassian.net/rest/api/2/issue/${jiraId} -o jira.json"""
                    responseJiraJson = readJSON file: "jira.json"
                    if (responseJiraJson != null) {
                        assigneeDetails = responseJiraJson.fields.assignee
                        if (assigneeDetails == "null") {
                            assignee = "Unassigned"
                        } else {
                            assignee = responseJiraJson.fields.assignee.displayName
                        }
                    }
                }
            }
        } catch(Exception e) {
            println e
        }
    }
}

def getBitbucketToken() {
    sh'curl -X POST -u "M5Pz4S2VDJK7c9fR8E:vxGjjGTTBv9Mm3jwH5GuMzZ2AKttAn9G" https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials > bitbucket_token.json'
    tokenJson = readJSON file: 'bitbucket_token.json'
    token = tokenJson.access_token
}

def dotNetCodeCompile(def compiledProjectName) {
    stage("Build Process") {
        try {
            sh"dotnet publish ${compiledProjectName}/${compiledProjectName}.csproj -c Release -o $WORKSPACE/"+repoName+"/publish/"
        } catch(e) {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Build process Failed","FAILED",true)
            error("Build Process failed")
        }
    }
}

def nodeCodeCompile() {
    stage("Build Process") {
        try {
            sh"""
            npm install
            npm run test
            """
        } catch(e) {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Build process Failed","FAILED",true)
            error("Build Process failed")
        }
    }
}

def crewVersionUpgrade()
    {
        checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-crew-ui'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-crew-ui.git"]]] 
                
        oldVersionJson = readJSON file: 'dxp-crew-ui/version.json'
        oldV = oldVersionJson.version
        def versionJson = oldV.tokenize('.')
        majorVersion = versionJson[0]
        minorVersion = versionJson[1]
        buildVersion = versionJson[2]
        version = Integer.parseInt(buildVersion) + 1
        newV =  "${majorVersion}.${minorVersion}.${version}"
        sh """sed -i 's|"${oldV}"|"${newV}"|g' dxp-crew-ui/version.json"""
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
        {
            sh(
            """
            cd dxp-crew-ui
            git add version.json
            git commit -m "New Version after apk build, DCP-44655 [skip ci]"
            git push git@bitbucket.org:decurtis/dxp-crew-ui.git HEAD:master
            """)
        }
    }
def guestVersionUpgrade()
    {
        checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-guest-ui'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-guest-ui.git"]]] 

        oldVersionJson = readJSON file: 'dxp-guest-ui/version.json'
        oldVer = oldVersionJson.version
        def versionJson = oldVer.tokenize('.')
        majorVersion = versionJson[0]
        minorVersion = versionJson[1]
        buildVersion = versionJson[2]
        version = Integer.parseInt(buildVersion) + 1
        newVer =  "${majorVersion}.${minorVersion}.${version}"
        sh """sed -i 's|"${oldVer}"|"${newVer}"|g' dxp-guest-ui/version.json"""
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
        {
            sh(
            """
            cd dxp-guest-ui
            git add version.json
            git commit -m "New Version after apk build, DCP-44655 [skip ci]"
            git push git@bitbucket.org:decurtis/dxp-guest-ui.git HEAD:master
            """)
        }
    }

def cabinTvVersionUpgrade()
    {
        checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-cabin-tv-ui'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-cabin-tv-ui.git"]]] 

        oldVersionJson = readJSON file: 'dxp-cabin-tv-ui/version.json'
        oldV = oldVersionJson.version
        def versionJson = oldV.tokenize('.')
        majorVersion = versionJson[0]
        minorVersion = versionJson[1]
        buildVersion = versionJson[2]
        version = Integer.parseInt(buildVersion) + 1
        newVerCabin =  "${majorVersion}.${minorVersion}.${version}"
        sh """sed -i 's|"${oldV}"|"${newVerCabin}"|g' dxp-cabin-tv-ui/version.json"""
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
        {
            sh(
            """
            cd dxp-cabin-tv-ui
            git add version.json
            git commit -m "New Version after apk build, DCP-44655 [skip ci]"
            git push git@bitbucket.org:decurtis/dxp-cabin-tv-ui.git HEAD:master
            """)
        }
    }

def cabinTabletVersionUpgrade()
    {
        checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-cabin-ui'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-cabin-ui.git"]]] 

        oldVersionJson = readJSON file: 'dxp-cabin-ui/version.json'
        oldV = oldVersionJson.version
        def versionJson = oldV.tokenize('.')
        majorVersion = versionJson[0]
        minorVersion = versionJson[1]
        buildVersion = versionJson[2]
        version = Integer.parseInt(buildVersion) + 1
        newVerCabin =  "${majorVersion}.${minorVersion}.${version}"
        sh """sed -i 's|"${oldV}"|"${newVerCabin}"|g' dxp-cabin-ui/version.json"""
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
        {
            sh(
            """
            cd dxp-cabin-ui
            git add version.json
            git commit -m "New Version after apk build, DCP-44655 [skip ci]"
            git push git@bitbucket.org:decurtis/dxp-cabin-ui.git HEAD:master
            """)
        }
    }

def buildScala(def process, def moduleName) {
    stage ("Build Process") {
        try {
            switch("${process}") {
                case "Scala-Assembly":
                    sh """
                        sbt scalafmt::test test:scalafmt::test scapegoat coverage test coverageReport
                        sbt assembly
                        cp target/scala-*/*.jar .
                    """
                    break
                case "Scala-Dist":
                    sh """
                        sbt clean dist
                        cp target/universal/*.zip .
                    """
                    break
                case "Scala-ReactNative":
                    sh '''
                        npm install
                        npm install webpack -g
                        npm install webpack-cli -g
                        MODULE_NAME='''+moduleName+'''
                        NODE_ENV='shell' webpack --mode production --env.TARGET=shell
                        MODULE_DIRECTORY=dist/'''+moduleName+'''
                        for js in dist/*.js
                        do
                            echo "${js}"
                            sed 's:/public:public:g' $js > ${js}.tmp
                            mv ${js}.tmp ${js}
                        done
                        mkdir dist/Data
                        find dist/* -not -name "Data" -exec mv {} dist/Data \\; || true
                        cp -R public dist/Data
                        mkdir $MODULE_DIRECTORY
                        mv dist/Data $MODULE_DIRECTORY
                        cd dist
                        zip -r9 '''+moduleName+'''.zip '''+moduleName+'''/*
                    '''
                    withCredentials([usernamePassword(credentialsId: 'NEXUS', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        sh """
                            cd dist
                            curl -v -u '${NEXUS_USER}:${NEXUS_PASSWORD}' --upload-file "${moduleName}".zip https://infra.dxp-decurtis.com/nexus/repository/maxiru-zip/"${moduleName}"/"${newVersion}"/"${moduleName}".zip
                        """
                    }
                    break
                case "Scala-ReactNative-Common":
                    sh '''
                        mkdir -p '''+moduleName+'''
                        cp -r ./dist '''+moduleName+'''/
                        zip -r9 '''+moduleName+'''.zip '''+moduleName+'''
                    '''
                    withCredentials([usernamePassword(credentialsId: 'NEXUS', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USER')]) {
                        sh("""curl -v -u '${NEXUS_USER}:${NEXUS_PASSWORD}' --upload-file "${moduleName}".zip https://infra.dxp-decurtis.com/nexus/repository/maxiru-zip/"${moduleName}"/"${newVersion}"/"${moduleName}".zip""")
                    }
                    break
                    default:
                    break
            }
        } catch(e) {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Build process Failed","FAILED",true)
            error("Build Process failed")
        }
    }
}

def buildApp(def appName)
{
    switch("${appName}")
    {
        case "CrewAndGuest":
            crewVersionUpgrade()
            apkCrewBuild()
            guestVersionUpgrade()
            apkGuestBuild()
        break
        case "Crew":
            crewVersionUpgrade()
            apkCrewBuild()
        break
        case "Guest":
            guestVersionUpgrade()
            apkGuestBuild()
        break
        case "Cabin-TV":
            cabinTvVersionUpgrade()
            apkCabinTvBuild()
        break
        case "Cabin-Tablet":
            cabinTabletVersionUpgrade()
            apkCabinTabletBuild()
        break
    }
}

def apkCrewBuildCommon()
    {
        sh"rm -rf dxp-shell-react-native"
        checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-shell-react-native'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-shell-react-native.git"]]] 

        env.MODULES_STORAGE_URL="http://209.50.56.182:8081"
        env.MODULES_STORAGE_PATH_TO_MODULES="/artifactory/modules/"
        env.CI_LOGIN="ci"
        env.CI_PASS="staysafe2019"
        env.COMPONENT_ID="38c1ab51-ce97-439c-9d20-c48d724e1fe5"
        env.COMPONENT_SECRET="G56TVYY78bjqdDYOh799"
        env.BUNDLE_ID="com.decurtis.dxp.crew"
        env.VERSION="${newV}"
        env.JDK="${tool 'JAVA8-Linux'}"
        env.PATH="${env.JDK}/bin:${env.PATH}"
        env.QA1_BASE_URL_SHIP="https://qa1-sagar.dxp-decurtis.com"
        env.PROD_BASE_URL_SHIP="https://dc-sagar.dxp-decurtis.com"
        env.INFRA_BASE_URL="https://dc-infra.dxp-decurtis.com"
        env.VV_DEV_DXP_BASE_URL_SHIP="https://application-dev.ship.virginvoyages.com/svc"
        env.VV_QC_DXP_BASE_URL_SHIP="https://application-integration.ship.virginvoyages.com/svc"
        env.VV_CERT_DXP_BASE_URL_SHIP="https://k8s-qaship.virginvoyages.com"
        env.VV_PROD_DXP_BASE_URL_SHIP="https://application.scl.virginvoyages.com/svc"
        env.DCL_DEV_BASE_URL_SHIP="https://dcl-dev-ship.dxp-decurtis.com"
        env.DCL_LATEST_BASE_URL_SHIP="https://dclshipdxpcore.wdw.disney.com"

        sh'''
        rm -rf dxp-shell-react-native/package-lock.json
        sed -i 's|maxiru|decurtis|g' dxp-shell-react-native/package.json
        sed -i 's|#53c1bda||g' dxp-shell-react-native/package.json
        sed -i 's|#333278701368aa5a816d9980c803e4ea8c639424||g' dxp-shell-react-native/package.json
        sed -i 's|MacKentoch|decurtis|g' dxp-shell-react-native/package.json
        sed -i 's|github.com|bitbucket.org|g' dxp-shell-react-native/package.json
        sed -i 's|https|ssh|g' dxp-shell-react-native/package.json
        '''
    }

def apkCrewBuild()
    {
        stage ("Building Crew App") 
        { 
            try
            {
                echo "DXP CREW APK"
                    apkCrewBuildCommon()
                    sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                        export ANDROID_HOME=/home/jenkins/android
                        export APP_TYPE=crew
                        export MODULES="AUTH@master, CREW@master, EMBARKATION@master, TABLERESERVATION@master, HOUSEKEEPING@master, DINING@develop, GUEST_SERVICE@develop, BARTENDER@develop, SERVER@develop, CPRO@develop, CDIR@develop, CFOL@develop, common/dxp-common-ui@master, common/dxp-crew-common-ui@master"
                        export GREETER_TYPE=EMBARK_GREETER
                        export ENV_PREFIX=DXP
                        export DEFAULT_CUSTOMER=DXP
                        export DEFAULT_PROXIMITY=true
                        export WITH_VLC=false

                        cd dxp-shell-react-native/scripts/jenkins
                        DXP_ENV="QA1" ELK_LOGS_URL="${INFRA_BASE_URL}/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${QA1_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${QA1_BASE_URL_SHIP}/dxpcore/" DEFAULT_PROXIMITY="true" ./build_prod_crew.sh
                        DXP_ENV="PROD" ELK_LOGS_URL="${INFRA_BASE_URL}/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${PROD_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${PROD_BASE_URL_SHIP}/configuration-service/" DEFAULT_PROXIMITY="true" ./build_prod_crew.sh
                        gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/crew-${newV}/
                        """
                    } 

                echo "VV CREW APK"
                    apkCrewBuildCommon()
                    sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                        export ANDROID_HOME=/home/jenkins/android
                        export MODULES="AUTH@master, CREW@master, EMBARKATION@master, TABLERESERVATION@master, HOUSEKEEPING@master, DINING@develop, GUEST_SERVICE@develop, BARTENDER@develop, SERVER@develop, CPRO@develop, CDIR@develop, CFOL@develop, common/dxp-common-ui@master, common/dxp-crew-common-ui@master, CHAT_CREW@develop, PEOPLE@develop, TRAC@develop, IMS@develop"
                        export APP_TYPE=crew
                        export GREETER_TYPE=EMBARK_GREETER
                        export ENV_PREFIX=DXP
                        export DEFAULT_THEME="crew.light"
                        export DEFAULT_CUSTOMER="VV"
                        export DEFAULT_PROXIMITY=true
                        export WITH_VLC=false
                        export MODULE_LOADER_URL="https://dxp-common-ui.simis.ai/src/images/loader.gif"

                        cd dxp-shell-react-native/scripts/jenkins
                        DXP_ENV="DEV" ELK_LOGS_URL="https://common-es.virginvoyages.com/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_DEV_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_DEV_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_crew.sh
                        DXP_ENV="QC" ELK_LOGS_URL="https://common-es.virginvoyages.com/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_QC_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_QC_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_crew.sh
                        DXP_ENV="CERT" ELK_LOGS_URL="https://common-es.virginvoyages.com/qa-ship-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_CERT_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_CERT_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_crew.sh
                        DXP_ENV="PROD" ELK_LOGS_URL="https://prodes.virginvoyages.com/prod-ship-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_PROD_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_PROD_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_crew.sh
                        gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/crew-${newV}/
                        """
                    }

                echo "DCL CREW APK"
                    apkCrewBuildCommon()
                    sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                        export ANDROID_HOME=/home/jenkins/android
                        export APP_TYPE=crew
                        export MODULES="AUTH@master, CREW@master, EMBARKATION@master, common/dxp-common-ui@master, common/dxp-crew-common-ui@master"
                        export ENV_PREFIX=DXP
                        export BRANCH=master
                        export DEFAULT_CUSTOMER="DCL"
                        export DEFAULT_PROXIMITY=true
                        export WITH_VLC=false
                        export MODULE_LOADER_URL="https://dxp-common-ui.simis.ai/src/images/loader_dcl.gif"

                        cd dxp-shell-react-native/scripts/jenkins
                        DXP_ENV="DEV" ELK_LOGS_URL="https://dcl-infra.dxp-decurtis.com/elasticsearch/mobile_framework/_doc" DEFAULT_THEME="dcl.crew.light" COMPONENT_AUTH_BASE_URL="${DCL_DEV_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${DCL_DEV_BASE_URL_SHIP}/dxpcore/" GREETER_TYPE="EMBARK_GREETER" ./build_prod_crew.sh
                        DXP_ENV="DEV_POC" ELK_LOGS_URL="https://dcl-infra.dxp-decurtis.com/elasticsearch/mobile_framework/_doc" DEFAULT_THEME="dcl.poc.light" COMPONENT_AUTH_BASE_URL="${DCL_DEV_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${DCL_DEV_BASE_URL_SHIP}/dxpcore/" GREETER_TYPE="POC_GREETER" ./build_prod_crew.sh
                        DXP_ENV="LATEST" ELK_LOGS_URL="" DEFAULT_THEME="dcl.crew.light" COMPONENT_AUTH_BASE_URL="${DCL_LATEST_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${DCL_LATEST_BASE_URL_SHIP}/dxpcore/" GREETER_TYPE="EMBARK_GREETER" ./build_prod_crew.sh
                        DXP_ENV="LATEST_POC" ELK_LOGS_URL="" DEFAULT_THEME="dcl.poc.light" COMPONENT_AUTH_BASE_URL="${DCL_LATEST_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${DCL_LATEST_BASE_URL_SHIP}/dxpcore/" GREETER_TYPE="POC_GREETER" ./build_prod_crew.sh
                        gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/crew-${newV}/
                        """
                    }
                    slackSend (channel: "#dxp-builds", color: '#00FF00', tokenCredentialId: 'slack-grafana-alert-token', message: '`Crew-'+newV+'`\nYou can find latest builds here - https://console.cloud.google.com/storage/browser/dxp-android-builds/maxiru/crew-'+newV+'/?project=eng-genius-224716')

            }
            catch(e) 
            {
                println e
                bitbucketBuildStatusNotify("Build Process","FAILED")
                error("Build Process failed")
            }
        }
    }

def apkGuestCommon()
    {
        sh"rm -rf dxp-shell-react-native"
        checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-shell-react-native'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-shell-react-native.git"]]] 

        env.MODULES_STORAGE_URL="http://209.50.56.182:8081"
        env.MODULES_STORAGE_PATH_TO_MODULES="/artifactory/modules/"
        env.CI_LOGIN="ci"
        env.CI_PASS="staysafe2019"
        env.COMPONENT_ID="38c1ab51-ce97-439c-9d20-c48d724e1fe5"
        env.COMPONENT_SECRET="G56TVYY78bjqdDYOh799"
        env.VV_PROD_COMPONENT_ID="38c1ab51-ce97-439c-9d20-c48d724e1fe5"
        env.VV_PROD_COMPONENT_SECRET="xHhmaOyMgODR4ty3"
        env.VV_STAGE_COMPONENT_ID="38c1ab51-ce97-439c-9d20-c48d724e1fe5"
        env.VV_STAGE_COMPONENT_SECRET="xHhmaOyMgODR4ty3"
        env.BUNDLE_ID="com.decurtis.dxp.guest"
        env.VERSION="${newVer}"
        env.JDK="${tool 'JAVA8-Linux'}"
        env.PATH="${env.JDK}/bin:${env.PATH}"
        env.QA1_BASE_URL_SHORE="https://qa1-shore.dxp-decurtis.com"
        env.PROD_BASE_URL_SHORE="https://dc-shore.dxp-decurtis.com"
        env.VV_QC_DXP_BASE_URL_SHORE="https://integration.virginvoyages.com/svc"
        env.VV_QA_DXP_BASE_URL_SHORE="https://qa.virginvoyages.com/svc"
        env.VV_STAGE_DXP_BASE_URL_SHORE="https://stage.virginvoyages.com/svc"
        env.VV_PROD_DXP_BASE_URL_SHORE="https://prod.virginvoyages.com/svc"

        sh'''
        rm -rf dxp-shell-react-native/package-lock.json
        sed -i 's|maxiru|decurtis|g' dxp-shell-react-native/package.json
        sed -i 's|#53c1bda||g' dxp-shell-react-native/package.json
        sed -i 's|#333278701368aa5a816d9980c803e4ea8c639424||g' dxp-shell-react-native/package.json
        sed -i 's|MacKentoch|decurtis|g' dxp-shell-react-native/package.json
        sed -i 's|github.com|bitbucket.org|g' dxp-shell-react-native/package.json
        sed -i 's|https|ssh|g' dxp-shell-react-native/package.json
        '''
    }

def apkGuestBuild()
    {
        stage ("Building Guest App") 
        { 
            try
            {                
                echo "DXP GUEST APK"
                    apkGuestCommon()
                    sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                        export ANDROID_HOME=/home/jenkins/android
                        export APP_TYPE=sailor_app
                        export MODULES="GUEST@master, common/dxp-common-ui@master, SHIPEATS@master"
                        export ENV_PREFIX=DXP
                        export DEFAULT_CUSTOMER=DXP
                        export BRANCH=master
                        export WITH_VLC=false

                        cd dxp-shell-react-native/scripts/jenkins
                        DXP_ENV="QC" DXP_BASE_URL="${QA1_BASE_URL_SHORE}" COMPONENT_AUTH_BASE_URL="${QA1_BASE_URL_SHORE}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${QA1_BASE_URL_SHORE}/dxpcore/" ./build_prod.sh
                        DXP_ENV="QA" DXP_BASE_URL="${PROD_BASE_URL_SHORE}" COMPONENT_AUTH_BASE_URL="${PROD_BASE_URL_SHORE}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${PROD_BASE_URL_SHORE}/configuration-service/" ./build_prod.sh
                        gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/guest-${newVer}/
                        """
                    } 

                echo "VV GUEST APK"
                    apkGuestCommon()
                    sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                        export ANDROID_HOME=/home/jenkins/android
                        export APP_TYPE=sailor_app
                        export MODULES="GUEST@master, common/dxp-common-ui@master, SHIPEATS@master, CHAT_GUEST@develop"
                        export ENV_PREFIX=DXP
                        export DEFAULT_CUSTOMER=VV
                        export MODULE_LOADER_URL=https://dxp-common-ui.simis.ai/src/images/loader.gif
                        export WITH_VLC=false
                        export DISABLE_APPLE_SIGN_IN=true

                        cd dxp-shell-react-native/scripts/jenkins
                        DXP_ENV="QC" ELK_LOGS_URL="https://common-es.virginvoyages.com/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_QC_DXP_BASE_URL_SHORE}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_QC_DXP_BASE_URL_SHORE}/dxpcore/" ./build_prod.sh
                        DXP_ENV="QA" ELK_LOGS_URL="https://common-es.virginvoyages.com/qa-sailor-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_QA_DXP_BASE_URL_SHORE}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_QA_DXP_BASE_URL_SHORE}/dxpcore/" ./build_prod_android_qa.sh
                        DXP_ENV="STAGE" ELK_LOGS_URL="https://stagees.virginvoyages.com/stage-sailor-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_STAGE_DXP_BASE_URL_SHORE}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_STAGE_DXP_BASE_URL_SHORE}/dxpcore/" ./build_prod_android_stage.sh
                        DXP_ENV="PROD" ELK_LOGS_URL="https://prodes.virginvoyages.com/prod-sailor-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_PROD_DXP_BASE_URL_SHORE}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_PROD_DXP_BASE_URL_SHORE}/dxpcore/" ./build_prod_android_prod.sh
                        gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/guest-${newVer}/
                        """
                    }
                    //slackSend (channel: "#dxp-builds", color: '#00FF00', tokenCredentialId: 'slack-grafana-alert-token', message: '`Guest-'+newVer+'`\nYou can find latest builds here - https://console.cloud.google.com/storage/browser/dxp-android-builds/maxiru/guest-'+newVer+'/?project=eng-genius-224716')
            }
            catch(e) 
            {
                println e
                bitbucketBuildStatusNotify("Build Process","FAILED")
                error("Build Process failed")
            }
        }
    }

def cabinTVCommon()
{
    sh"rm -rf dxp-shell-react-native"
    checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-shell-react-native'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-shell-react-native.git"]]] 

    sh"""
    rm -rf dxp-shell-react-native/package-lock.json
    sed -i 's|maxiru|decurtis|g' dxp-shell-react-native/package.json
    sed -i 's|#53c1bda||g' dxp-shell-react-native/package.json
    sed -i 's|#333278701368aa5a816d9980c803e4ea8c639424||g' dxp-shell-react-native/package.json
    sed -i 's|MacKentoch|decurtis|g' dxp-shell-react-native/package.json
    sed -i 's|github.com|bitbucket.org|g' dxp-shell-react-native/package.json
    sed -i 's|https|ssh|g' dxp-shell-react-native/package.json
    """

    env.MODULES_STORAGE_URL="http://209.50.56.182:8081"
    env.MODULES_STORAGE_PATH_TO_MODULES="/artifactory/modules/"
    env.CI_LOGIN="ci"
    env.CI_PASS="staysafe2019"
    env.COMPONENT_ID="38c1ab51-ce97-439c-9d20-c48d724e1fe5"
    env.COMPONENT_SECRET="G56TVYY78bjqdDYOh799"
    env.VERSION="${newVerCabin}"
    env.JDK="${tool 'JAVA8-Linux'}"
    env.PATH="${env.JDK}/bin:${env.PATH}"

    env.VV_DEV_DXP_BASE_URL_SHIP="https://application-dev.ship.virginvoyages.com/svc"
    env.VV_QC_DXP_BASE_URL_SHIP="https://application-integration.ship.virginvoyages.com/svc"
    env.VV_CERT_DXP_BASE_URL_SHIP="https://k8s-qaship.virginvoyages.com"
    env.VV_PROD_DXP_BASE_URL_SHIP="https://application.scl.virginvoyages.com/svc"

    env.APP_TYPE="cabin_tv"
    env.MODULES="CTV@master"
    env.ENV_PREFIX="DXP"
    env.DEFAULT_CUSTOMER="VV"
    env.MODULE_LOADER_URL="https://dxp-common-ui.simis.ai/src/images/loader.gif"
    env.WITH_VLC="true"
}

def apkCabinTvBuild()
{
    stage ("Building Cabin TV App") 
    { 
        try
        {
            cabinTVCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="DEV" ELK_LOGS_URL="https://common-es.virginvoyages.com/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_DEV_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_DEV_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin_tv.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintv-${newVerCabin}/
                """
            }
        
            cabinTVCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="QC" ELK_LOGS_URL="https://common-es.virginvoyages.com/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_QC_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_QC_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin_tv.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintv-${newVerCabin}/
                """
            }
        
            cabinTVCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="CERT" ELK_LOGS_URL="https://common-es.virginvoyages.com/qa-ship-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_CERT_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_CERT_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin_tv.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintv-${newVerCabin}/
                """
            }
        
            cabinTVCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="PROD" ELK_LOGS_URL="https://prodes.virginvoyages.com/prod-ship-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_PROD_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_PROD_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin_tv.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintv-${newVerCabin}/
                """
            }
            slackSend (channel: "#dxp-builds", color: '#00FF00', tokenCredentialId: 'slack-grafana-alert-token', message: '`Cabin TV-'+newVerCabin+'`\nYou can find latest builds here - https://console.cloud.google.com/storage/browser/dxp-android-builds/maxiru/cabintv-'+newVerCabin+'/?project=eng-genius-224716')
        }
        catch(e) 
            {
                println e
                bitbucketBuildStatusNotify("Build Process","FAILED")
                error("Build Process failed")
            }
    }
}

def cabinTabletCommon()
{
    sh"rm -rf dxp-shell-react-native"
    checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-shell-react-native'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-shell-react-native.git"]]] 

    sh"""
    rm -rf dxp-shell-react-native/package-lock.json
    sed -i 's|maxiru|decurtis|g' dxp-shell-react-native/package.json
    sed -i 's|#53c1bda||g' dxp-shell-react-native/package.json
    sed -i 's|#333278701368aa5a816d9980c803e4ea8c639424||g' dxp-shell-react-native/package.json
    sed -i 's|MacKentoch|decurtis|g' dxp-shell-react-native/package.json
    sed -i 's|github.com|bitbucket.org|g' dxp-shell-react-native/package.json
    sed -i 's|https|ssh|g' dxp-shell-react-native/package.json
    """

    env.MODULES_STORAGE_URL="http://209.50.56.182:8081"
    env.MODULES_STORAGE_PATH_TO_MODULES="/artifactory/modules/"
    env.CI_LOGIN="ci"
    env.CI_PASS="staysafe2019"
    env.COMPONENT_ID="38c1ab51-ce97-439c-9d20-c48d724e1fe5"
    env.COMPONENT_SECRET="G56TVYY78bjqdDYOh799"
    env.VERSION="${newVerCabin}"
    env.JDK="${tool 'JAVA8-Linux'}"
    env.PATH="${env.JDK}/bin:${env.PATH}"

    env.VV_DEV_DXP_BASE_URL_SHIP="https://application-dev.ship.virginvoyages.com/svc"
    env.VV_QC_DXP_BASE_URL_SHIP="https://application-integration.ship.virginvoyages.com/svc"
    env.VV_CERT_DXP_BASE_URL_SHIP="https://k8s-qaship.virginvoyages.com"
    env.VV_PROD_DXP_BASE_URL_SHIP="https://application.scl.virginvoyages.com/svc"

    env.APP_TYPE="cabin"
    env.MODULES="CABINCONTROL@master"
    env.ENV_PREFIX="DXP"
    env.DEFAULT_CUSTOMER="VV"
    env.MODULE_LOADER_URL="https://dxp-common-ui.simis.ai/src/images/loader.gif"
    env.WITH_VLC=""
}

def apkCabinTabletBuild()
{
    stage ("Building Cabin Tablet App") 
    { 
        try
        {
            cabinTabletCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="DEV" ELK_LOGS_URL="https://common-es.virginvoyages.com/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_DEV_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_DEV_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintablet-${newVerCabin}/
                """
            }
        
            cabinTabletCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="QC" ELK_LOGS_URL="https://common-es.virginvoyages.com/elasticsearch/mobile_framework/_doc" COMPONENT_AUTH_BASE_URL="${VV_QC_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_QC_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintablet-${newVerCabin}/
                """
            }
        
            cabinTabletCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="CERT" ELK_LOGS_URL="https://stagees.virginvoyages.com/stage-ship-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_CERT_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_CERT_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintablet-${newVerCabin}/
                """
            }
        
            cabinTabletCommon()
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh"""
                export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
                export ANDROID_HOME=/home/jenkins/android
            
                cd dxp-shell-react-native/scripts/jenkins
                DXP_ENV="PROD" ELK_LOGS_URL="https://prodes.virginvoyages.com/prod-ship-mobileapplogs/_doc" COMPONENT_AUTH_BASE_URL="${VV_PROD_DXP_BASE_URL_SHIP}/identityaccessmanagement-service/" CONFIGURATION_BASE_URL="${VV_PROD_DXP_BASE_URL_SHIP}/dxpcore/" ./build_prod_cabin.sh
                gsutil -m cp $WORKSPACE/*/dxp-shell-react-native/apk-output/*.apk gs://dxp-android-builds/maxiru/cabintablet-${newVerCabin}/
                """
            }
            slackSend (channel: "#dxp-builds", color: '#00FF00', tokenCredentialId: 'slack-grafana-alert-token', message: '`Cabin Tablet-'+newVerCabin+'`\nYou can find latest builds here - https://console.cloud.google.com/storage/browser/dxp-android-builds/maxiru/cabintablet-'+newVerCabin+'/?project=eng-genius-224716')
        }
        catch(e) 
            {
                println e
                bitbucketBuildStatusNotify("Build Process","FAILED")
                error("Build Process failed")
            }
    }
}

def dockerBuildProcess(def imageName,def env) {
    stage ("Build Docker Image and Push") {
        if(env) {
            def environment = env.split(',')
            for (client in environment)
            {
                dockerImageProcess(imageName, client)
            }
        } else {
            dockerImageProcess(imageName, "ALL")
        }
    }
}

def androidCodeCompilePriority() {
    try {
        env.JDK="${tool 'JAVA8-Linux'}"
        env.PATH="${env.JDK}/bin:${env.PATH}"
        sh '''
            export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
            export ANDROID_HOME=/home/jenkins/android
            mkdir apks

            ./gradlew clean assembleVVProdDebug
            ./gradlew assembleDCLcertDebug
            ./gradlew assembleDCLstageDebug

            cp aci/build/outputs/apk/*/debug/*.apk apks/
            cp gangway/build/outputs/apk/*/debug/*.apk apks/

            ./gradlew clean assembleVVprodRelease
            ./gradlew assembleDCLcertRelease
            ./gradlew assembleDCLstageRelease

            cp aci/build/outputs/apk/*/release/*.apk apks/
            cp gangway/build/outputs/apk/*/release/*.apk apks/
        '''
        androidApkUpload()
    } catch(e) {
        println e
        bitbucketBuildStatusNotify("Build Process","FAILED")
        insertCIStatus("Build process Failed","FAILED",true)
        error("Build Process failed")
    }
}

def androidCodeCompileOthers() {
    try {
        env.JDK="${tool 'JAVA8-Linux'}"
        env.PATH="${env.JDK}/bin:${env.PATH}"
        sh '''
            export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
            export ANDROID_HOME=/home/jenkins/android

            ./gradlew clean assembleDXPdevDebug
            ./gradlew assembleDXPqa1Debug
            ./gradlew assembleDXPprodDebug
            ./gradlew assembleVVdevDebug
            ./gradlew assembleVVqa1Debug
            ./gradlew assembleVVcertDebug
            ./gradlew assembleNCLdevDebug
            ./gradlew assembleNCLqa1Debug
            ./gradlew assembleNCLqaonpremDebug
            ./gradlew assembleDCLdevDebug
            ./gradlew assembleDCLqa1Debug

            cp aci/build/outputs/apk/*/debug/*.apk apks/
            cp gangway/build/outputs/apk/*/debug/*.apk apks/

            ./gradlew clean assembleDXPdevRelease
            ./gradlew assembleDXPqa1Release
            ./gradlew assembleDXPprodRelease
            ./gradlew assembleVVdevRelease
            ./gradlew assembleVVqa1Release
            ./gradlew assembleVVcertRelease
            ./gradlew assembleNCLdevRelease
            ./gradlew assembleNCLqa1Release
            ./gradlew assembleNCLqaonpremRelease
            ./gradlew assembleDCLdevRelease
            ./gradlew assembleDCLqa1Release

            cp aci/build/outputs/apk/*/release/*.apk apks/
            cp gangway/build/outputs/apk/*/release/*.apk apks/
        '''
        androidApkUpload()
    } catch(e) {
        println e
        bitbucketBuildStatusNotify("Build Process","FAILED")
        insertCIStatus("Build process Failed","FAILED",true)
        error("Build Process failed")
    }
}

def androidApkUpload() {
    try {
        sh """
            gsutil -m cp apks/*.apk gs://dxp-android-builds/android-local/${newVersion}/
        """
        deploymentStatus = 'true'
        sendEmail(null)
    } catch(e) {
        println e
        bitbucketBuildStatusNotify("Build Process","FAILED")
        insertCIStatus("Build process Failed","FAILED",true)
        error("Build Upload failed")
    }
}

def nodeCommonLibCodeCompile() {
    stage("Build Process") {
        try {
            sh """
                npm install
                npm run test
                npm publish
            """
        } catch(e)  {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Build process Failed","FAILED",true)
            error("Build Process failed")
        }
    }
}

def reactCodeCompile(def env,def helm) {
    stage("Build Docker Image and Push") {
        try {
            println env
            if(env!="ALL" && helm!="dxppaymentweb") {
                def environment = env.split(',')
                for (client in environment) {
                    sh"sudo rm -rf "+repoName+"/dist || true"
                    sh"npm run build ${client}"
                    dockerImageProcess(imageName,client)
                }
            } else if (env!="ALL" && helm=="dxppaymentweb") {
                def environment = env.split(',')
                for (client in environment) {
                    sh"sudo rm -rf "+repoName+"/dist || true"
                    sh"npm run build"
                    dockerImageProcess(imageName,client)
                }
            }
            else {
                println "else"
                sh"npm run build"
                dockerImageProcess(imageName, "ALL")
            }
        } catch(e) {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Docker image build and push Failed","FAILED",true)
            error("Docker image build and push failed.")
        }
    }
}

def versionUpgrade(def isAdapterEnabled, def helmName, def technology, def branch, def dbName) {
    stage("Version Update")
    {
        try
        {
            startBuild = sh(returnStdout: true, script: 'date +%s').toInteger() 
            switch("${technology}")
            {
                case "Java":
                        oldVersion = readMavenPom().getVersion()
                        oldPOMVersion = getPomVersion("${isAdapterEnabled}")
                        def BUILD_NUMB  = Integer.parseInt(oldPOMVersion[4]) + 1;
                        newVersion = "${oldPOMVersion[1]}.${oldPOMVersion[2]}.${oldPOMVersion[3]}-${BUILD_NUMB}"
                        if("${isAdapterEnabled}" == 'YES')
                        {
                            sh"""
                            cd ${repoName}
                            mvn -B versions:set -DgenerateBackupPoms=false -DnewVersion=${newVersion}"""
                        }
                        sh"mvn -B versions:set -DgenerateBackupPoms=false -DnewVersion=${newVersion}"
                        dbVersionUpdate("${dbName}")
                        versionUpgradeInternal(branch,technology)
                    break
                case "Node":
                case "Node-Common":
                case "React":
                        oldVersionJson = readJSON file: 'package.json'
                        oldVersion = oldVersionJson.version
                        sh "npm --no-git-tag-version version patch"
                        newVersionJson = readJSON file: 'package.json'
                        newVersion = newVersionJson.version
                        versionUpgradeInternal(branch,technology)
                    break
                case "DotNet":
                        sh'''
                        vars1=$(cat Directory.Build.props | grep "<Version>" | cut -d"<" -f2 | cut -d">" -f2)
                        vars2=$(echo $vars1 | awk -F"." '{print $1 "." $2 "."}')
                        vars3=$(cat Directory.Build.props | grep "<Version>" | cut -d"<" -f2 | cut -d">" -f2 | cut -d"." -f3)
                        vars4=$(echo $vars3 | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}')
                        vars5=$(echo $vars2$vars4)
                        sed -i 's|'"$vars1"'|'"$vars5"'|g' Directory.Build.props
                        '''
                        newVersion = sh(script: 'cat Directory.Build.props | grep "<Version>" | cut -d"<" -f2 | cut -d">" -f2', returnStdout: true)
                        newVersion = newVersion.trim()
                        versionUpgradeInternal(branch,technology)
                    break
                case "Scala":
                        oldVersionJson = readJSON file: 'version.json'
                        oldVersion = oldVersionJson.version
                        def versionJson = oldVersion.tokenize('.')
                        majorVersion = versionJson[0]
                        minorVersion = versionJson[1]
                        buildVersion = versionJson[2]
                        version = Integer.parseInt(buildVersion) + 1
                        newVersion =  "${majorVersion}.${minorVersion}.${version}"
                        sh """sed -i 's|"${oldVersion}"|"${newVersion}"|g' version.json"""
                        dbVersionUpdate("${dbName}")
                        versionUpgradeInternal(branch,technology)
                    break
                case "Android":
                        oldVersionJson = readJSON file: 'version.json'
                        oldVersion = oldVersionJson.version
                        def versionJson = oldVersion.tokenize('.')
                        majorVersion = versionJson[0]
                        minorVersion = versionJson[1]
                        buildVersion = versionJson[2]
                        version = Integer.parseInt(buildVersion) + 1
                        newVersion =  "${majorVersion}.${minorVersion}.${version}"
                        sh """sed -i 's|"${oldVersion}"|"${newVersion}"|g' version.json"""
                        versionUpgradeInternal(branch,technology)
                    break
                case "Python":
                        oldVersionJson = readJSON file: 'version.json'
                        oldVersion = oldVersionJson.version
                        def versionJson = oldVersion.tokenize('.')
                        majorVersion = versionJson[0]
                        minorVersion = versionJson[1]
                        buildVersion = versionJson[2]
                        version = Integer.parseInt(buildVersion) + 1
                        newVersion =  "${majorVersion}.${minorVersion}.${version}"
                        sh """sed -i 's|"${oldVersion}"|"${newVersion}"|g' version.json"""
                        dbVersionUpdate("${dbName}")
                        versionUpgradeInternal(branch,technology)
                    break
                case "NodeMaxiru":
                        oldVersionJson = readJSON file: 'version.json'
                        oldVersion = oldVersionJson.version
                        def versionJson = oldVersion.tokenize('.')
                        majorVersion = versionJson[0]
                        minorVersion = versionJson[1]
                        buildVersion = versionJson[2]
                        version = Integer.parseInt(buildVersion) + 1
                        newVersion =  "${majorVersion}.${minorVersion}.${version}"
                        sh """sed -i 's|"${oldVersion}"|"${newVersion}"|g' version.json"""
                        versionUpgradeInternal(branch,technology)
                    break
                default:
                    break
            }
        }
        catch(e)
        {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Version Update Failed","FAILED",true)
            error("Version Update failed")
        }
    }
}

def javaCodeCompile(def javaVersion) {
    stage("Build Process")
    {
        try
        {
            switch("${javaVersion}")
            {
                case "Java-8":
                        tool name: 'JAVA8', type: 'jdk'
                        sh"""
                        mvn clean test
                        mvn clean install
                        """
                    break
                case "Java-10":
                        tool name: 'JAVA10', type: 'jdk'
                        sh"""
                        mvn clean test
                        mvn clean install
                        """
                    break
                case "Java-11":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean test
                        mvn clean install
                        """
                    break
                case "Java-Common":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean test
                        mvn clean deploy"""
                    break
                case "Core-Java-11":
                        sh"""
                        export JAVA_HOME=/usr/lib/jvm/jdk-11.0.1
                        mvn clean test
                        mvn clean install assembly:single"""
                    break
                default:
                    echo "No Java Version passed"
                    break
            }
        }
        catch(e)
        {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Build process Failed","FAILED",true)
            error("Build Process failed")
        }
    }
}

def sonarAnalysis(def technology, def compiledProjectName, def slnName) {
    stage ("Sonar Analysis")
    {
        try
        {
            withSonarQubeEnv('SonarServer')
            {
                switch("${technology}")
                {
                    case "Java":
                            sh"mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.7.0.1746:sonar"
                        break
                    case "Node":
                    case "React":
                    case "Node-Common":
                    case "NodeMaxiru":
                            sh"sonar-scanner"
                        break
                    case "DotNet":
                            sh"""
                            dotnet test ${compiledProjectName}.Tests/${compiledProjectName}.Tests.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=$WORKSPACE/
                            dotnet-sonarscanner begin /k:"${compiledProjectName}" /d:sonar.cs.opencover.reportsPaths=$WORKSPACE/coverage.opencover.xml
                            dotnet build ${slnName}
                            dotnet-sonarscanner end
                            """
                        break
                    case "Scala":
                            sh"sonar-scanner"
                        break
                    case "Python":
                            sh"sonar-scanner"
                        break
                    case "Android":
                            sh"gradle sonarqube"
                        break
                    default:
                            echo "No Technology passed for Scanning Sonar"
                        break
                }
            }
        }
        catch(e)
        {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Sonar Analysis failed","FAILED",true)
            error("Sonar Analysis failed")
        }
    }
}

def qualityGate() {
    stage ("Quality Gate")
    {
        try
        {
            timeout(time: 5, unit: 'MINUTES')
            {
                    def qg = waitForQualityGate()
                    if (qg.status != 'OK')
                    {
                    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                    }
            }
        }
        catch(e)
        {
                println e
                bitbucketBuildStatusNotify("Build Process","FAILED")
                insertCIStatus("Quality Gate failed","FAILED",true)
                error("Quality Gate failed")
        }
    }
}

/*
    This method is required to move compiled Jar to root directory. Parameters
    isAdapterEnabled = If Component is build with Adapters
    bitBucketRepoName = IF Adapter is enabled then this value should not be null
*/
def jarMoveProcess(def isAdapterEnabled) {
    if ("${isAdapterEnabled}" == 'YES' && repoName != null)
    {
        sh "mv $WORKSPACE/"+repoName+"/"+repoName+"/target/*.jar $WORKSPACE/"+repoName+""
    }
    else
    {
        sh "mv $WORKSPACE/"+repoName+"/target/*.jar $WORKSPACE/"+repoName+""
    }
}

def dockerImageProcess(def imageName, def env) {
        try
        {
            image="$DECURTIS_REGISTRY_PATH/${imageName}:${newVersion}".trim()
            if(env == 'null')
            {
                env = "ALL"
            }
            switch(env)
            {
                case "DCL":
                case "DXP":
                docker.withRegistry("https://"+image, "DECURTIS-REGISTRY-CREDENTIALS")
                {
                    def dockerImage = docker.build(image,"$WORKSPACE/"+repoName)
                    dockerImage.push()
                }
                break
                case "VV":
                docker.withRegistry("https://$VV_REGISTRY_PATH", "VV-REGISTRY-CREDENTIALS")
                {
                    def dockerImage = docker.build("$VV_REGISTRY_PATH/${imageName}:${newVersion}".trim(),"$WORKSPACE/"+repoName)
                    dockerImage.push()
                }
                break
                case "ALL":
                docker.withRegistry("https://"+image, "DECURTIS-REGISTRY-CREDENTIALS")
                {
                    def dockerImage = docker.build(image,"$WORKSPACE/"+repoName)
                    dockerImage.push()
                    sh"""
                        docker tag ${image} $NCL_REGISTRY_PATH/${imageName}:${newVersion}
                        docker tag ${image} $NCL_QA1_REGISTRY_PATH/${imageName}:${newVersion}
                        docker tag ${image} $VV_REGISTRY_PATH/${imageName}:${newVersion}
                    """
                    docker.withRegistry("http://$VV_REGISTRY_PATH", "VV-REGISTRY-CREDENTIALS")
                    {
                        sh "docker push $VV_REGISTRY_PATH/${imageName}:${newVersion}"
                    }
                }
                break
            }
        }
        catch(e)
        {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Docker image build and push Failed","FAILED",true)
            error("Docker image build and push failed.")
        }
}

def helmProcess(def helmName, def imageName, def deploymentEnvironment, def technology) {
    stage ("Helm Process")
    {
        try
        {
            if("${helmName}" !=  'null')
            {
                checkout scm: [$class: 'GitSCM', branches: [[name: 'master']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-helm-charts'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-helm-charts.git"]]]
                dir("$WORKSPACE/"+repoName+"/dxp-helm-charts/dxp-components")
                {
                    def chartYaml = readYaml file: "${helmName}/Chart.yaml"
                    chartYaml.version = "${newVersion}".trim()
                    sh "rm ${helmName}/Chart.yaml"
                    writeYaml file: "${helmName}/Chart.yaml", data: chartYaml
                    sh"""
                    helm package ${helmName}
                    helm gcs init gs://dxp-helm-charts/${helmName}/
                    helm repo add gcs-helm-${helmName} gs://dxp-helm-charts/${helmName}
                    helm gcs push ${helmName}-${newVersion}.tgz gs://dxp-helm-charts/${helmName}
                    """
                }
                checkout scm: [$class: 'GitSCM', branches: [[name: 'dev']], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dxp-helm-values'], [$class: 'UserExclusion', excludedUsers: 'ciduser']], userRemoteConfigs: [[credentialsId: 'Jenkins-Bitbucket-Ssh-Key', url: "git@bitbucket.org:decurtis/dxp-helm-values.git"]]]
                dir("$WORKSPACE/"+repoName+"/dxp-helm-values")
                {
                    if(environments.contains("DXP") || environments.contains("ALL")){
                        fileshore = "values_shore.yaml"
                        filesagar = "values_sagar.yaml"
                        updateVersioninValuesFile(deploymentEnvironment)
                    }
                    else
                    {
                        if(environments.contains("NCL"))
                        {
                            fileshore = "ncl_shore.yaml"
                            filesagar = "ncl_ship.yaml"
                            updateVersioninValuesFile(deploymentEnvironment)
                        }
                        if(environments.contains("VV"))
                        {
                            fileshore = "vv_shore.yaml"
                            filesagar = "vv_ship.yaml"
                            updateVersioninValuesFile(deploymentEnvironment)
                        }

                        if(environments.contains("DCL"))
                        {
                            fileshore = "dcl_shore.yaml"
                            filesagar = "dcl_ship.yaml"
                            updateVersioninValuesFile(deploymentEnvironment)
                        }
                    }
                }
            }
        }
        catch(e)
        {
            println e
            bitbucketBuildStatusNotify("Build Process","FAILED")
            insertCIStatus("Helm process Failed","FAILED",true)
            error("Helm process failed.")
        }
    }
}

def updateVersioninValuesFile(def deploymentEnvironment) {
    try
    {
        switch("${deploymentEnvironment}")
        {
            case "ALL":
                updateComponentVersionInShore(helmName, imageName, fileshore)
                updateComponentVersionInSagar(helmName, imageName, filesagar)
                break
            case "Shore":
                updateComponentVersionInShore(helmName, imageName, fileshore)
                break
            case "Ship":
                updateComponentVersionInSagar(helmName, imageName, filesagar)
                break
        }
        if(! environments.contains("DXP") && ! environments.contains("ALL"))
        {
            dir("$WORKSPACE/"+repoName+"/dxp-helm-values")
            {
                sshagent(['Jenkins-Bitbucket-Ssh-Key'])
                {
                    sh(
                    """
                    git add  *.yaml
                    git commit -m "New Version, DCP-44655 [skip ci]"
                    git pull git@bitbucket.org:decurtis/dxp-helm-values.git HEAD:dev
                    git push --force git@bitbucket.org:decurtis/dxp-helm-values.git HEAD:dev
                    """)
                }
            }
        }
    }
    catch(Exception e)
    {
        println e
        bitbucketBuildStatusNotify("Build Process","FAILED")
        insertCIStatus("Helm process Failed","FAILED",true)
        error("Helm process failed.")
    }
}

def dxpDatabaseBuild(def dbName) {
    try
    {
        if("${dbName}" != 'null' && "${dbName}" != '')
        {
            sh"""
            tar -zcvf ${dbName}-${newVersion}.tar.gz ${dbName}
            gsutil cp  ${dbName}-${newVersion}.tar.gz gs://dxp-databases-scripts/${dbName}/
            """
        }
    }
    catch(e)
    {
        println e
        bitbucketBuildStatusNotify("Build Process","FAILED")
        insertCIStatus("Database Scripts Build Failed","FAILED",true)
        error("Database Scripts Build failed.")
    }
}


def dxpDeployment(def componentName, def environment)  {
    stage("Component Deployment")
    {
        try
        {
            if("${componentName}" != 'null')
            {
                switch("${environment}")
                {
                    case "ALL":
                    deploymentDXPShip(componentName)
                    deploymentDXPShore(componentName)
                    break
                    case "Shore":
                    deploymentDXPShore(componentName)
                    break
                    case "Ship":
                    deploymentDXPShip(componentName)
                    break
                    default:
                    echo "Environment doesn't exist with configuration"
                    break
                }
                sh"helm repo remove gcs-helm-${componentName}"
            }
        }
        catch(e)
        {
            println e
            bitbucketBuildStatusNotify("Deployment Process","FAILED")
            insertCIStatus("Component Deployment Failed","FAILED",false)
            error("Component Deployment failed")
        }
    }
}

def bitbucketBuildStatusNotify(def statusKey, def status) {
    status_url = "https://api.bitbucket.org/2.0/repositories/decurtis/"+repoName+"/commit/"+commitId.trim()+"/statuses/build"
    def message
    def jenkinsJobUrl = "https://infra.dxp-decurtis.com/jenkins/job/"+jobName+"/"+buildNumber+"/console"
    switch("${status}")
    {
        case "INPROGRESS":
        message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is InProgress.", "url":"'+jenkinsJobUrl+'"}'
        break
        case "SUCCESSFUL":
        message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is Successful.", "url":"'+jenkinsJobUrl+'"}'
        break
        case "FAILED":
        message = '{"key":" '+statusKey+'","state": "'+status+'","name":" '+statusKey+' is failed.", "url":"'+jenkinsJobUrl+'"}'
        break
        default:
        break
    }
    def post = new URL(status_url).openConnection();
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Authorization", "Bearer "+token);
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(message.getBytes("UTF-8"));
    def statusCode = post.getResponseCode();
    if(statusCode != 200 && statusCode != 201)
    {
        print('An error has occurred while notifying to BitBucket.')
        print(statusCode)
    }
}



/* Internal Methods (Not for external use)*/
def getPomVersion(def isAdapterEnabled) {
    def matcher
    if("${isAdapterEnabled}" == 'YES')
    {
        matcher = readFile("$WORKSPACE/"+repoName+"/pom.xml") =~ '<version>(\\d*)\\.(\\d*)\\.(\\d*)-(\\d*)</version>'
    }
    else
    {
        matcher = readFile('pom.xml') =~ '<version>(\\d*)\\.(\\d*)\\.(\\d*)-(\\d*)</version>'
    }

    matcher ? matcher[0] : null
}

def dbVersionUpdate(def dbName) {
    if(dbName != 'null' && dbName != '')
    {

            dir("$WORKSPACE/"+repoName+"/${dbName}/UpgradeScripts/Scripts")
            {
                def sql= readFile "UpdateComponentVersion.sql"
                def vars1 = sql[0..<sql.indexOf("BEGIN")].split("\n").findAll {it.startsWith("DECLARE")}.findAll{it.contains('current_version')}.toString()
                def version1= vars1.split(":=")
                def prevVersion = version1[1].replaceAll("'", "").replaceAll(";", "").replaceAll("]", "").trim().toString()
                echo "${prevVersion}"
                sh"""sed -i 's|${prevVersion}|${newVersion}|g' UpdateComponentVersion.sql
                """
            }
    }
}

def versionUpgradeInternal(def branch, def technology) {
    switch(technology)
    {
        case "Java":
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add . -- ':!DXP-Automation' ':!bitbucket_token.json' ':!file.json' ':!jira.json'
                git commit -m "New Version, DCP-44655 [skip ci]"
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:master
                """)
            }
        break
        case "Node":
        case "React":
        case "Node-Common":
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add $WORKSPACE/${repoName}/package.json
                git commit -m "New Version, DCP-44655 [skip ci]"
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:master
                """)
            }
        break
        case "DotNet":
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add Directory.Build.props
                git commit -m "New Version, DCP-44655 [skip ci]"
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:master
                """)
            }
        break
        case "Scala":
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add . -- ':!DXP-Automation' ':!bitbucket_token.json' ':!file.json' ':!jira.json'
                git commit -m "New Version, DCP-44655 [skip ci]"
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push --force --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:master
                """)
            }
        break
        case "Python":
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add . -- ':!DXP-Automation' ':!bitbucket_token.json' ':!file.json' ':!jira.json'
                git commit -m "New Version, DCP-44655 [skip ci]"
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push --force --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:master
                """)
            }
        break
        case "Android":
        case "NodeMaxiru":
        sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add version.json
                git commit -m "New Version, DCP-44655 [skip ci]"
                git tag -a ${newVersion} -m 'Version ${newVersion}'
                git push --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:master
                """)
            }
        break
        default:
        break
    }
}
def bitbucketTagging(def branchName) {
    try
    {
        stage("BitBucket Version Tagging")
        {
            sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                    git tag -a ${newVersion} -m 'Version ${newVersion}'
                    git push  --force --follow-tags git@bitbucket.org:decurtis/${repoName}.git HEAD:${branchName}
                """)
            }
        }
    }
    catch(e)
    {
        println e
        bitbucketBuildStatusNotify("Deployment Process","FAILED")
        insertCIStatus("BitBucket Version Tagging Failed","FAILED",false)
        error("BitBucket Version Tagging failed")
    }
}

def updateComponentVersionInShore(def helmName, def imageName, def filename) {
    def shoreValues = readYaml file: "${filename}"
    shoreValues.global.imageVersion."${helmName}" = "${imageName}:${newVersion}"
    sh "rm ${filename}"
    writeYaml file: "${filename}", data: shoreValues
}

def updateComponentVersionInSagar(def helmName, def imageName, def filename) {
    def sagarValues = readYaml file: "${filename}"
    sagarValues.global.imageVersion."${helmName}" = "${imageName}:${newVersion}"
    sh "rm ${filename}"
    writeYaml file: "${filename}", data: sagarValues
}

def deploymentDXPShip(def componentName) {
    sh"""
    export KUBECONFIG=/home/jenkins/DXP/dxp-gcp-dev.conf
    helm repo update gcs-helm-${componentName}
    helm upgrade --install  ${componentName}-dev-sagar gcs-helm-${componentName}/${componentName} --values $WORKSPACE/${repoName}/dxp-helm-values/values_sagar.yaml --version ${newVersion} --namespace sagar --force
    """
}

def deploymentDXPShore(def componentName) {
    sh"""
    export KUBECONFIG=/home/jenkins/DXP/dxp-gcp-dev.conf
    helm repo update gcs-helm-${componentName}
    helm upgrade --install ${componentName}-dev-shore gcs-helm-${componentName}/${componentName} --values $WORKSPACE/${repoName}/dxp-helm-values/values_shore.yaml --version ${newVersion} --namespace shore --force
    """
}

def pushUpgradedHelmVersion(def deploymentEnvironment, def technology) {
    try {
        dir("$WORKSPACE/"+repoName+"/dxp-helm-charts/dxp-components")
        {
            def chartYaml = readYaml file: "${helmName}/Chart.yaml"
            chartYaml.version = "${newVersion}".trim()
            sh "rm ${helmName}/Chart.yaml"
            writeYaml file: "${helmName}/Chart.yaml", data: chartYaml
            sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                git add ${helmName}/Chart.yaml
                git commit -m "DCP-44655 Updating ${helmName} Helm Chart version number [skip ci]"
                git pull git@bitbucket.org:decurtis/dxp-helm-charts.git HEAD:master
                git push --force git@bitbucket.org:decurtis/dxp-helm-charts.git HEAD:master --tags
                """)
            }

        } 
        dir("$WORKSPACE/"+repoName+"/dxp-helm-values") {
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh(
                """
                git stash
                git pull git@bitbucket.org:decurtis/dxp-helm-values.git HEAD:dev
                """)
            }

            updateVersioninValuesFile(deploymentEnvironment)
            sshagent(['Jenkins-Bitbucket-Ssh-Key']) {
                sh(
                """
                git add  *.yaml
                git commit -m "New Version, DCP-44655 [skip ci]"
                git push --force git@bitbucket.org:decurtis/dxp-helm-values.git HEAD:dev
                """)
            }
        }
    } catch(e) {
        println e
        bitbucketBuildStatusNotify("Test","FAILED")
        insertCIStatus("Helm Version Upgrade Failed","FAILED",false)
        error("Version Upgrade Failed.")
    }
}

def insertIntoPostgres() {
    try {
        dir("$WORKSPACE/"+repoName+"/DXP-Automation"){
            def currentDate=new Date().format('MM-dd-yyyy HH:mm:ss')
            def username=authorEmail.toLowerCase().split("@")[0]
            def user = username.replace("."," ").capitalize()
            println user
            sh """
                chmod 777 DXP-CI/UpdateSQL.sh
                currentDate="${currentDate}" developerName="${user}" jiraId="${jiraId}" Repository="${bitBucketRepoName}" version="${newVersion}" assignee="${assignee}" commitid="${commitId}" DXP-CI/UpdateSQL.sh
            """
        }
    } catch(Exception e) {
        println e
        bitbucketBuildStatusNotify("Test","FAILED")
    }
}

def insertCIStatus(def message, def status, def calculateBuildEnd){
    try{
        if(status.equalsIgnoreCase("FAILED")){
            sshagent(['Jenkins-Bitbucket-Ssh-Key'])
            {
                sh(
                """
                    git tag -d ${newVersion}
                    git push git@bitbucket.org:decurtis/${repoName}.git :refs/tags/${newVersion}
                """)
            }
        }
        def currentDate=new Date().format('MM-dd-yyyy HH:mm:ss')
        endtime=sh(returnStdout: true, script: 'date +%s').toInteger()
        def username=authorEmail.toLowerCase().split("@")[0]
        def user = username.replace("."," ").capitalize()
        if(calculateBuildEnd == true){
            getEndBuildTime()
        }
        dir("$WORKSPACE/"+repoName+"/DXP-Automation"){
            float totalTime=((endtime - starttime)/60)
            totalTime=totalTime.round(2)
            float totalBuildTime =((endBuild - startBuild)/60)
            totalBuildTime=totalBuildTime.round(2)
            sh """
                chmod 777 DXP-CI/UpdateCIStatus.sh
                currentDate="${currentDate}" jobnumber="${env.BUILD_NUMBER}" developerName="${user}" Repository="${bitBucketRepoName}" compilationtime="${totalBuildTime}" totaltime="${totalTime}" status="${status}" message="${message}" DXP-CI/UpdateCIStatus.sh
            """
            def slackuser=authorEmail.toLowerCase().split("@")[0]
            def notify_message = "@${slackuser} Please find below status of your CI job: \n *${status}*: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'(${env.BUILD_URL}) \n *Component Name*: ${bitBucketRepoName} \n *Reason*: ${message} "
            if (status == 'SUCCESS') {
                color = 'GREEN'
                colorCode = '#00FF00'
            } else {
                color = 'RED'
                colorCode = '#FF0000'
            }
            slackSend (channel: "@${slackuser}", color: colorCode, tokenCredentialId: 'slack-grafana-alert-token', message: notify_message)
        }
    }
    catch(Exception e){
        println e
        bitbucketBuildStatusNotify("Test","FAILED")
    }
}

def get_slack_user_id_from_email(emailId) {
    withCredentials([string(credentialsId: 'slack-api-token', variable: 'SLACK_TOKEN')]) {
        def url = "https://slack.com/api/users.lookupByEmail?email=${emailId}"
        def response = httpRequest ignoreSslErrors: true,
            httpMode: 'GET',
            contentType: 'APPLICATION_JSON',
            quiet: true,
            customHeaders: [[name: 'Authorization', value: "Bearer ${SLACK_TOKEN}"]],
            url: url
        def respContent = readJSON text: response.content
        def userId = respContent['user']['id']
        return "${userId}"
    }
}

def slackFailureNotification() {
    //Need to check if not in use will delete.
}

return this